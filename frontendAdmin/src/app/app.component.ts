import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { DynamicFileAdminServiceService } from 'src/app/dynamic-file-service.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent  {
  title = 'frontendAdmin';
  constructor(public router:Router) {  }
 
}
