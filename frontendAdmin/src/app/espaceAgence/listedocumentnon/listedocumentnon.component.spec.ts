import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListedocumentnonComponent } from './listedocumentnon.component';

describe('ListedocumentnonComponent', () => {
  let component: ListedocumentnonComponent;
  let fixture: ComponentFixture<ListedocumentnonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListedocumentnonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListedocumentnonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
