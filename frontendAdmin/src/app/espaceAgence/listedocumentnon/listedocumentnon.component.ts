import { Component, OnInit } from '@angular/core';
import { DynamicFileAdminServiceService } from 'src/app/dynamic-file-service.service';
import { FileService } from 'src/app/services/file.service';
import { ClientService} from '../../services/client.service';
import {Client} from '../../interface/client';
import { TokenStorageService } from '../../services/token-storage.service';
import swal from 'sweetalert2';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CommonModule } from "@angular/common";

@Component({
  selector: 'app-listedocumentnon',
  templateUrl: './listedocumentnon.component.html',
  styleUrls: ['./listedocumentnon.component.css']
})
export class ListedocumentnonComponent implements OnInit {

  constructor(private tokenStorage: TokenStorageService,private clientservice:ClientService,private dynamicScriptLoader: DynamicFileAdminServiceService,private service:FileService) { }
  files=[];
  clients:Client[];
  InvalidFrom;
  ngOnInit(): void {
    this.getAll();
    this.loadScripts();  
    this.getAllclient();
  }
  save(f){
    f.agence=this.tokenStorage.getUser().Agenceid;
    this.service.add(f)
    .subscribe(data =>{
     this.succes();
       },
    
    error => {//console.log(error);
     this.InvalidFrom=error["error"]["message"]; 
     console.log(error);
   });
   }
  
  getAllclient(){
    this.clientservice.allClient().subscribe(data=>{          
      this.clients=data; 
      console.log(this.clients);  
   })
  }
    public getAll(){
   
      return this.service.search().subscribe(data=>{  
        console.log(data);
        this.files=data;
        console.log(data);
       })
    }

  
    public delete(id){
     
  
  
      swal.fire({
        title: 'Êtes-vous sûr?',
        text: "Vous ne pourrez pas revenir en arrière!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Oui, supprimé!'
      }).then((result) => {
      
        if (result.value) {
          return this.service.delete(id).subscribe(data=>{
            swal.fire(
              'Effacé!',
              'Votre Document a été supprimé.',
              'success'
            ).then((r) => {
              location.reload();
            })
          })
          
    
        }
       // location.reload();  
    
      
    })
    }

    succes(): void{
      swal.fire(
        'document!',
        'votre document est ajouter!',
        'success'
      ).then((r) => {
        location.reload();
      })
    }
    private loadScripts() {
      this.dynamicScriptLoader.load('c10','c6','c2','c4','c1').then(data => {
      }).catch(error => console.log(error));
    }

}
