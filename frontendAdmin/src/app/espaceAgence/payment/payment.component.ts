import { Component, OnInit } from '@angular/core';
import { DynamicFileAdminServiceService } from 'src/app/dynamic-file-service.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TokenStorageService } from '../../services/token-storage.service';
import swal from 'sweetalert2';
import { CommonModule } from "@angular/common";
import { PayementService} from '../../services/payement.service';
import { Payement} from '../../interface/payement';
import { ContratService} from '../../services/contrat.service';
import {Contrat} from '../../interface/contrat';
import { FactureService} from '../../services/facture.service';
import {Facture} from '../../interface/facture';
@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit {
  payments:Payement[]=[];
  payment:Payement={
    id: "",
    date: null,
    montant: "",
    mode: "",
    description: "",
    Userid: this.tokenStorage.getUser().id,
    agence: this.tokenStorage.getUser().Agenceid,
    facture: "",
    contrat:""
  }
  contrat:Contrat;
  prix;
  facture : Facture;
  montant=0;
  constructor(private service: FactureService,private contartservice:ContratService,private tokenStorage: TokenStorageService,private payementService: PayementService,private dynamicScriptLoader: DynamicFileAdminServiceService) { }

  ngOnInit(): void {
    this.loadScripts();
    this.getAll();

  }
  getAll(){
    this.payementService.all().subscribe(data=>{          
      this.payments=data; 
      console.log(this.payments);   
   })
 }


 public delete(id:String){
   


  swal.fire({
    title: 'Êtes-vous sûr?',
    text: "Vous ne pourrez pas revenir en arrière!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Oui, supprimé!'
  }).then((result) => {
  
    if (result.value) {
      return this.payementService.supprimer(id).subscribe(data=>{
        swal.fire(
          'Effacé!',
          'Votre Payment a été supprimé.',
          'success'
        ).then((r) => {
          location.reload();
        })
      })
      

    }
   // location.reload();  

  
})
}


getOne(id){
  this.payementService.getPayementById(id).subscribe(data=>{
    
    this.payment=data;   
    this.payment.facture=data['FactureId'];
    this.payment.contrat=data['ContratId'];
    this.prix=data.montant;
 })
}

public update(payment){
  
  if(this.payment.facture==null){
    this.contartservice.getContratById(this.payment.contrat).subscribe(data1=>{
      this.contrat=data1;   
this.montant=parseInt(this.contrat.paye)-parseInt(this.prix)+parseInt(this.payment.montant);

this.contrat.paye=this.montant.toString();
this.contartservice.updatepaye(this.contrat,this.contrat.id)
.subscribe(data4 =>{ 
  console.log(data4);
})


   })
  }
  else{
    this.service.getFactureById(this.payment.facture).subscribe(data2=>{
    
      this.facture=data2;   
      this.montant=parseInt(this.facture.paye)-parseInt(this.prix)+parseInt(this.payment.montant);
      this.facture.paye=this.montant.toString();
      this.service.updatepaye(this.facture,this.facture.id)
      .subscribe(data5 =>{ 
        console.log(this.montant);
        console.log(this.facture);
        console.log(data5);
      })
   })
  }
  
  this.payementService.updatePayement(this.payment.id, this.payment)
  .subscribe(
    response => {
      console.log(response);
      this.modifier();
      
    },
    error => {
      console.log(error);
    });
    
}


modifier(): void{
  swal.fire(
    'Payement!',
    'votre payement est modifier!',
    'success'
  ).then((r) => {
    location.reload();
  })
}

  private loadScripts() {
    this.dynamicScriptLoader.load('c10','c6','c2','c4','c1','c16').then(data => {
    }).catch(error => console.log(error));
  }

}
