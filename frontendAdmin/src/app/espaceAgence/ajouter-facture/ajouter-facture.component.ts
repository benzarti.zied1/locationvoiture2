import { Component, OnInit } from '@angular/core';
import { DynamicFileAdminServiceService } from 'src/app/dynamic-file-service.service';
import { FactureService} from '../../services/facture.service';
import {Facture} from '../../interface/facture';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TokenStorageService } from '../../services/token-storage.service';
import swal from 'sweetalert2';
import { ClientService} from '../../services/client.service';
import {Client} from '../../interface/client';
import { ArticleService} from '../../services/article.service';
import {Article} from '../../interface/article';
@Component({
  selector: 'app-ajouter-facture',
  templateUrl: './ajouter-facture.component.html',
  styleUrls: ['./ajouter-facture.component.css']
})
export class AjouterFactureComponent implements OnInit {
  facture : Facture={
    id: "",
    date: "",
    delai : "",
    Prixtotal: "",
    paye: "",
    client:"",
    Userid: this.tokenStorage.getUser().id,
    agence: this.tokenStorage.getUser().Agenceid
  }
  articles:Article[]=[];
  article: Article={
    id: "",
    nom: "",
    prix: "",
    tva: "", 
    prixttc:"",
    cout:"",
    Userid: this.tokenStorage.getUser().id,
    agence:this.tokenStorage.getUser().Agenceid
  }
  IsFormNonValid=false;
  clients:Client[];
  selectedValue:number;
  tarticle =[];
  tqte=[];
  tprix=[];
  prixtotal=0;
  prix=0;
counter = Array;

  constructor(private articleservice: ArticleService,private clientservice:ClientService,private tokenStorage: TokenStorageService,private service: FactureService,private dynamicScriptLoader: DynamicFileAdminServiceService) { }


  ngOnInit() {
   
    
     this.loadScripts();
     this.getAllclient();
     this.getAllarticle();
  }

  getAllclient(){
    this.clientservice.allClient().subscribe(data=>{          
      this.clients=data; 
      console.log(this.clients);  
   })
  }
  getAllarticle(){
    this.articleservice.all().subscribe(data=>{          
      this.articles=data; 
   })
  }
  save(f) {
   f.Prixtotal=this.prixtotal;
   f.Userid= this.tokenStorage.getUser().id,
    f.agence= this.tokenStorage.getUser().Agenceid
    this.service.add(f)
     .subscribe(data =>{ console.log(data);
      this.succes();
      
    
     
        },
     
     error => {console.log(error);
      this.IsFormNonValid=true;
    });

}

succes(): void{
  swal.fire(
    'Facture!',
    'votre Facture est ajoute!',
    'success'
  ).then((r) => {
    location.reload();
  })
}


calfacture(f){
  this.prixtotal=0;
  this.prix=0;
 console.log(f);
 this.tarticle=[];
 this.tqte=[];
 for (var key in f) 
 {
 if(key.indexOf('article')> -1)
 {
     
      this.tarticle.push(f[key]);
 }
 if(key.indexOf('qte')> -1)
 {
     
      this.tqte.push(f[key]);
 }
}

for ( var j=0;j<this.tarticle.length;j++){
  
  for ( var k=0;k<this.articles.length;k++){
    if(this.articles[k].id===this.tarticle[j]){
      console.log(this.articles[k].prixttc);
      console.log(this.tqte[j]);
      this.prixtotal=this.prixtotal+ (parseInt(this.articles[k].prixttc) *  parseInt(this.tqte[j])) ;
      this.prix=this.prix+(parseInt(this.articles[k].prix) *  parseInt(this.tqte[j]));
    }

  }


console.log(this.prixtotal);

}


 
  }


getOnearticle(id){
    this.articleservice.getarticleById(id).subscribe(data=>{
      
      this.article=data;   
      console.log(this.article);
   })
  }
  // js

  private loadScripts() {
    this.dynamicScriptLoader.load('c10','c6','c2','c4','c1','c16','c12','c13').then(data => {
    }).catch(error => console.log(error));
  }

}
