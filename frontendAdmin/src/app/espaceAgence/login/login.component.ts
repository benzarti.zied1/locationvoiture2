import { Component, OnInit } from '@angular/core';
import { AgenceService} from '../../services/agence.service';
import {Agence} from '../../interface/agence';
import { Router } from '@angular/router';
import { TokenStorageService } from '../../services/token-storage.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  form: any = {};
  roles: string[] = [];
  errorMessage = '';
  isLoggedIn = false;
  isLoginFailed = false;

  isactive = false;


  user: Agence = {
    email: '',
    username: '',
    password:'',
    active:0,
    nom:'',
    tel:'',
    adressepostale:'',
    ville:'',
    tva:'',
    compteboncaire:'',
    image:'',
  };
  constructor(private service:AgenceService,private router: Router,private tokenStorage: TokenStorageService) { }

  ngOnInit(): void {
    
  }
  /*
  login(user:Agence) {
    
    console.log(user)
    this.service.login(this.user)
     .subscribe(data =>{ console.log(data);
      this.tokenStorage.saveToken(data.accessToken);
      this.tokenStorage.saveUser(data);
      this.lienmenu();
      
        },
     
     error => console.log(error));

   
  }
*/

onSubmit() {
  this.service.login1(this.form).subscribe(
    data => {
      if(data.active == null){
        this.isactive=true;
      }
      else{
        this.tokenStorage.saveToken(data.accessToken);
        this.tokenStorage.saveUser(data);
  
        this.isLoginFailed = false;
        this.isLoggedIn = true;
        this.roles = this.tokenStorage.getUser().roles;
        if(this.roles[0]=="ROLE_ADMIN"){
        this.lienadmin();
        }
        else{
          this.lienmenu();
        }
       
      }
     
    },
    err => {
      this.errorMessage = err.error.message;
      this.isLoginFailed = true;
    }
  );
}
  lienmenu(){
    this.router.navigate(['Menu']);
  }

  lienadmin(){
    this.router.navigate(['Listeagence']);
  }
}
