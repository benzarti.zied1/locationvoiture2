import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListeVoitureNonDispoComponent } from './liste-voiture-non-dispo.component';

describe('ListeVoitureNonDispoComponent', () => {
  let component: ListeVoitureNonDispoComponent;
  let fixture: ComponentFixture<ListeVoitureNonDispoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListeVoitureNonDispoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListeVoitureNonDispoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
