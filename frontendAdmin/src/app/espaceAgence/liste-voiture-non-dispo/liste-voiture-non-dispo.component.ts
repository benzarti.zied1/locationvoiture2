import { Component, OnInit } from '@angular/core';
import { DynamicFileAdminServiceService } from 'src/app/dynamic-file-service.service';
import { VoitureService} from '../../services/voiture.service';
import {Voiture} from '../../interface/voiture';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TokenStorageService } from '../../services/token-storage.service';
import swal from 'sweetalert2';


@Component({
  selector: 'app-liste-voiture-non-dispo',
  templateUrl: './liste-voiture-non-dispo.component.html',
  styleUrls: ['./liste-voiture-non-dispo.component.css']
})
export class ListeVoitureNonDispoComponent implements OnInit {
  voitures: Voiture[]=[];
  constructor(private dynamicScriptLoader: DynamicFileAdminServiceService,private tokenStorage: TokenStorageService,private service: VoitureService) { }
  
  ngOnInit() {
   
    
    this.loadScripts();
    this.getAll();
 }

 getAll(){
  this.service.allNonDispo().subscribe(data=>{
    this.voitures=data;    
 })
}

public Disponible(voiture:Voiture,id:String){
  return this.service.updateDisponible(voiture,id).subscribe(()=>{
    location.reload();
    this.getAll();

})
}

  // js

  private loadScripts() {
    this.dynamicScriptLoader.load('c10','c6','c2','c4','c1').then(data => {
    }).catch(error => console.log(error));
  }

}
