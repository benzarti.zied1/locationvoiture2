import { Component, OnInit } from '@angular/core';
import { DynamicFileAdminServiceService } from 'src/app/dynamic-file-service.service';
import { TokenStorageService } from '../../services/token-storage.service';
import { EmployeService} from '../../services/employe.service';
import {Employe} from '../../interface/employe';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import swal from 'sweetalert2';



@Component({
  selector: 'app-liste-user',
  templateUrl: './liste-user.component.html',
  styleUrls: ['./liste-user.component.css']
})
export class ListeUserComponent implements OnInit {
  users: any[]=[];
u;

  employe: Employe = {
    username:'',
    email:'',
    password:'',
    active:1,
    AgenceId:this.tokenStorage.getUser().id

  };
  agence: string;
  IsFormNonValid=false;
  a=true;

  constructor(private service: EmployeService,   private dynamicScriptLoader: DynamicFileAdminServiceService,private tokenStorage: TokenStorageService) { }
 


  ngOnInit() {
    this.loadScripts();
    this.getAll();
    console.log(this.tokenStorage.getUser().Agenceid);

  }

  

  save(employe: EmployeService) {
    //this.employe.AgenceId=this.tokenStorage.getUser().id;
    //console.log(employe)
    this.service.add(this.employe)
     .subscribe(data =>{ console.log(data);
      this.succes();
      this.employe= {
        username:'',
        email:'',
        password:'',
        active:1,
        AgenceId:this.tokenStorage.getUser().Agenceid
    
      };
      location.reload();  

        },
     
     error => {console.log(error);
      this.IsFormNonValid=true;
    });
  }
  
   getAll(){
     this.service.all().subscribe(data=>{
       this.users=data;
       //console.log(data.length)
       /*
       for (let i=0;i<data.length;i++){
    
        this.employe= {
        username:'hhh',
        email:'hhh',
        password:'',
        active:1,
        AgenceId:this.tokenStorage.getUser().id
    
      }

      this.users.push(this.employe);
      
     console.log(this.users);
    
       }
       */
     
 

    })
  }


  public delete(id:String){
   


      swal.fire({
        title: 'Êtes-vous sûr?',
        text: "Vous ne pourrez pas revenir en arrière!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Oui, supprimé!'
      }).then((result) => {
      
        if (result.value) {
          return this.service.supprimer(id).subscribe(data=>{
            swal.fire(
              'Effacé!!',
              'Votre employe a été supprimé.',
              'success'
            ).then((r) => {
              location.reload();
            })
          })
          

        }
       // location.reload();  

      
    })
  }

  public active(employe:Employe,id:String){
    return this.service.active(employe,id).subscribe(()=>{
      location.reload();

  })
}

public desactive(employe:Employe,id:String){
  return this.service.desactive(employe,id).subscribe(()=>{
    location.reload();

})
}




  // js

  private loadScripts() {
    this.dynamicScriptLoader.load('c10','c6','c2','c4','c1').then(data => {
    }).catch(error => console.log(error));
  }

  succes(): void{
    swal.fire(
      'Utilisateur!',
      'Employe est ajoute!',
      'success'
    ).then((r) => {
      location.reload();
    })
    


  

  }

 

}
