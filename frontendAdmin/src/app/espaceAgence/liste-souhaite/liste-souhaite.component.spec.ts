import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListeSouhaiteComponent } from './liste-souhaite.component';

describe('ListeSouhaiteComponent', () => {
  let component: ListeSouhaiteComponent;
  let fixture: ComponentFixture<ListeSouhaiteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListeSouhaiteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListeSouhaiteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
