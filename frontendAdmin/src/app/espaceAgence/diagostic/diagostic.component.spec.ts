import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DiagosticComponent } from './diagostic.component';

describe('DiagosticComponent', () => {
  let component: DiagosticComponent;
  let fixture: ComponentFixture<DiagosticComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiagosticComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiagosticComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
