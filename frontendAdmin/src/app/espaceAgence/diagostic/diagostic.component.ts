import { Component, OnInit } from '@angular/core';
import { DynamicFileAdminServiceService } from 'src/app/dynamic-file-service.service';
import { VoitureService} from '../../services/voiture.service';
import { DiagnostiquesService} from '../../services/diagnostiques.service';
import {Voiture} from '../../interface/voiture';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TokenStorageService } from '../../services/token-storage.service';
import swal from 'sweetalert2';
import { CommonModule } from "@angular/common";
import { Diagnostiques} from '../../interface/diagnostiques';
import { GarageService} from '../../services/garage.service';
import { Garage} from '../../interface/garage';

@Component({
  selector: 'app-diagostic',
  templateUrl: './diagostic.component.html',
  styleUrls: ['./diagostic.component.css']
})
export class DiagosticComponent implements OnInit {
  voitures:Voiture[]=[];
  garages:Garage[]=[];

  diagnostiques:Diagnostiques[]=[];
  IsFormNonValid=false;
  diagnostique:Diagnostiques={
    id: "",
    voitureImmatricule: "",
    prix: "",
    description: "",
    carage: "",
    date: null, 
    Userid: this.tokenStorage.getUser().id,
    agence:this.tokenStorage.getUser().Agenceid
  }
  id="";



  constructor(private garageService:GarageService,private diagnostiquesService: DiagnostiquesService,private dynamicScriptLoader: DynamicFileAdminServiceService,private tokenStorage: TokenStorageService,private service: VoitureService) { }


  ngOnInit() {
   
    
     this.loadScripts();
     this.getAllVoiture();
     this.getAll();
     this.getAllGarage();

  }
  getAllGarage(){
    this.garageService.all().subscribe(data=>{          
      this.garages=data; 
      console.log(this.garages);   
   })
 }
  getAllVoiture(){
    this.service.allDispo().subscribe(data=>{          
      this.voitures=data; 
      console.log(this.voitures);   
   })
 }
 save(diagnostique :Diagnostiques) {
  this.diagnostiquesService.add(this.diagnostique)
  .subscribe(data =>{ 
    console.log(data);
    diagnostique={
      id: "",
      voitureImmatricule: "",
      prix: "",
      description: "",
      carage: "",
      date: null, 
      Userid: this.tokenStorage.getUser().id,
      agence:this.tokenStorage.getUser().Agenceid
    }
   this.succes();
   

     },
  
  error => {console.log(error);
   this.IsFormNonValid=true;
 });
}

getAll(){
  this.diagnostiquesService.all().subscribe(data=>{          
    this.diagnostiques=data; 
 })
}

public delete(id:String){
   


  swal.fire({
    title: 'Êtes-vous sûr?',
    text: "Vous ne pourrez pas revenir en arrière!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Oui, supprimé!'
  }).then((result) => {
  
    if (result.value) {
      return this.diagnostiquesService.supprimer(id).subscribe(data=>{
        swal.fire(
          'Effacé!',
          'Votre diagnostiques a été supprimé.',
          'success'
        ).then((r) => {
          location.reload();
        })
      })
      

    }
   // location.reload();  

  
})
}

getOne(id){
  this.diagnostiquesService.getdiagnostiquesById(id).subscribe(data=>{
    
    this.diagnostique=data;   
 })
}

public update(diagnostique){
  this.diagnostiquesService.updatediagnostiques(this.diagnostique.id, this.diagnostique)
  .subscribe(
    response => {
      console.log(response);
      this.modifier();
      
    },
    error => {
      console.log(error);
    });
}

initi():void{
  this.diagnostique={
    id: "",
    voitureImmatricule: "",
    prix: "",
    description: "",
    carage: "",
    date: null, 
    Userid: this.tokenStorage.getUser().id,
    agence:this.tokenStorage.getUser().Agenceid
  }
 }
 succes(): void{
  swal.fire(
    'Diagnostiques!',
    'votre diagnostiques est ajouter!',
    'success'
  ).then((r) => {
    location.reload();
  })
}

modifier(): void{
  swal.fire(
    'Diagnostiques!',
    'votre diagnostiques est modifier!',
    'success'
  ).then((r) => {
    location.reload();
  })
}
  // js

  private loadScripts() {
    this.dynamicScriptLoader.load('c10','c6','c2','c4','c1','c16').then(data => {
    }).catch(error => console.log(error));
  }

}
