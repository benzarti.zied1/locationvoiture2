import { Component, OnInit } from '@angular/core';
import { DynamicFileAdminServiceService } from 'src/app/dynamic-file-service.service';
import { VoitureService} from '../../services/voiture.service';
import {Voiture} from '../../interface/voiture';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TokenStorageService } from '../../services/token-storage.service';
import swal from 'sweetalert2';
import { CommonModule } from "@angular/common";


@Component({
  selector: 'app-liste-user-agence',
  templateUrl: './liste-user-agence.component.html',
  styleUrls: ['./liste-user-agence.component.css']
})
export class ListeUserAgenceComponent implements OnInit {
  
  voitures:Voiture[]=[];
  
 

  constructor(private dynamicScriptLoader: DynamicFileAdminServiceService,private tokenStorage: TokenStorageService,private service: VoitureService) { }


  ngOnInit() {
    //this.loadScripts();
    this.getAll();
 }

 getAll(){
   this.service.allDispo().subscribe(data=>{          
      
     this.voitures=data; 
     console.log(this.voitures);   
  })
}
  


  

 // js


}
