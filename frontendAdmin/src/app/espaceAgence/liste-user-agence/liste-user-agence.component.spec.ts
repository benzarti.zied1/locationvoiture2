import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListeUserAgenceComponent } from './liste-user-agence.component';

describe('ListeUserAgenceComponent', () => {
  let component: ListeUserAgenceComponent;
  let fixture: ComponentFixture<ListeUserAgenceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListeUserAgenceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListeUserAgenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
