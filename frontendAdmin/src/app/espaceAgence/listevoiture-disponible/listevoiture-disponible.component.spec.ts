import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListevoitureDisponibleComponent } from './listevoiture-disponible.component';

describe('ListevoitureDisponibleComponent', () => {
  let component: ListevoitureDisponibleComponent;
  let fixture: ComponentFixture<ListevoitureDisponibleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListevoitureDisponibleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListevoitureDisponibleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
