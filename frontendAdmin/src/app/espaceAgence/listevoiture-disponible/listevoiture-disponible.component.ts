import { Component, OnInit } from '@angular/core';
import { DynamicFileAdminServiceService } from 'src/app/dynamic-file-service.service';
import { VoitureService} from '../../services/voiture.service';
import {Voiture} from '../../interface/voiture';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TokenStorageService } from '../../services/token-storage.service';
import swal from 'sweetalert2';
import { MaintenanceService} from '../../services/maintenance.service';
import { Maintenance} from '../../interface/maintenance';
import { KilometrageService} from '../../services/kilometrage.service';
import { Kilometrage} from '../../interface/kilometrage';
import { CarburantService} from '../../services/carburant.service';
import { Carburant} from '../../interface/carburant';
import { DiagnostiquesService} from '../../services/diagnostiques.service';
import { Diagnostiques} from '../../interface/diagnostiques';
import { TraqueursService} from '../../services/traqueurs.service';
import { Traqueurs} from '../../interface/traqueurs';
import { VistetechniqueService} from '../../services/vistetechnique.service';
import { Vistetechnique} from '../../interface/vistetechnique';
import { GarageService} from '../../services/garage.service';
import { Garage} from '../../interface/garage';






@Component({
  selector: 'app-listevoiture-disponible',
  templateUrl: './listevoiture-disponible.component.html',
  styleUrls: ['./listevoiture-disponible.component.css']
})
export class ListevoitureDisponibleComponent implements OnInit {
  voitures: Voiture[]=[];
  garages:Garage[]=[];


  voiture: Voiture={
    id: "",
    immatricule: "",
    annee: "",
    marque: "",
    modele: "",
    categoire: "",
    boite: "",
    clim: "",
    nbporte: 0,
    nbpersonne: 0,
    consommation: "",
    image: "",
    assurance: "",
    datededebutassurance: null,
    datefinassurance: null,
    prixachat: "",
    dateachat: null,
    datemiseencerculation: null,
    disponible: 1,
    Userid: this.tokenStorage.getUser().id,
    Agence: this.tokenStorage.getUser().Agenceid
  }

  maintenance : Maintenance= {
    id: "",
    voitureImmatricule: "",
    description: "",
    datedebut: null,
    dateretour: null,
    Heuredebut: "",
    minutedebut: "",
    heureretour: "",
    minuteretour: "",
    prix: "",
    carage:"",
    Userid: this.tokenStorage.getUser().id,
    agence:this.tokenStorage.getUser().Agenceid

  }
  kilometrage : Kilometrage= {
    id: "",
    voitureImmatricule: "",
    kilometrage: "",
    date: null, 
    Userid: this.tokenStorage.getUser().id,
    agence:this.tokenStorage.getUser().Agenceid

  }
  carburant: Carburant={
    id: "",
    voitureImmatricule: "",
    carburant: "",
    date: null, 
    Userid: this.tokenStorage.getUser().id,
    agence:this.tokenStorage.getUser().Agenceid
  }
  diagnostique:Diagnostiques={
    id: "",
    voitureImmatricule: "",
    prix: "",
    description: "",
    carage: "",
    date: null, 
    Userid: this.tokenStorage.getUser().id,
    agence:this.tokenStorage.getUser().Agenceid
  }
  traqueur:Traqueurs={
    id: "",
    voitureImmatricule: "",
    identifiant: "",
    fournisseur: "",
    Userid: this.tokenStorage.getUser().id,
    agence:this.tokenStorage.getUser().Agenceid
  }
  vistetechnique : Vistetechnique= {
    id: "",
    voitureImmatricule: "",
    description: "",
    kilometrage:"",
    prix: "",
    carage: "",    
    date: null,
    Userid: this.tokenStorage.getUser().id,
    agence:this.tokenStorage.getUser().Agenceid

  }
  id:"";
  IsFormNonValid=false;

  constructor(private garageService:GarageService,private vistetechniqueService: VistetechniqueService,private traqueursService: TraqueursService,private diagnostiquesService: DiagnostiquesService,private carburantService: CarburantService,private kilometrageService: KilometrageService,private maintenanceService: MaintenanceService,private dynamicScriptLoader: DynamicFileAdminServiceService,private tokenStorage: TokenStorageService,private service: VoitureService) { }


  ngOnInit() {
   
    
   
    this.loadScripts();
     this.getAll();
     this.getAllGarage();
     
  }

  getAllGarage(){
    this.garageService.all().subscribe(data=>{          
      this.garages=data; 
      console.log(this.garages);   
   })
 }
  getAll(){
    this.service.allDispo().subscribe(data=>{
      this.voitures=data;    
      console.log(this.voitures);
   })
 }

 getOne(id){
  this.service.getVoitureById(id).subscribe(data=>{
    this.voiture={
      id: "",
      immatricule: "",
      annee: "",
      marque: "",
      modele: "",
      categoire: "",
      boite: "",
      clim: "",
      nbporte: 0,
      nbpersonne: 0,
      consommation: "",
      image: "",
      assurance: "",
      datededebutassurance: null,
      datefinassurance: null,
      prixachat: "",
      dateachat: null,
      datemiseencerculation: null,
      disponible: 1,
      Userid: this.tokenStorage.getUser().id,
      Agence: this.tokenStorage.getUser().Agenceid
    }
    this.voiture=data;   
    console.log(this.voiture); 
 })
}

savemaintenance(maintenance :Maintenance) {
  this.maintenance.voitureImmatricule=this.voiture.immatricule;
  this.maintenanceService.add(this.maintenance)
  .subscribe(data =>{ 
    maintenance = {
      id: "",
      voitureImmatricule: "",
      description: "",
      datedebut: null,
      dateretour: null,
      Heuredebut: "",
      minutedebut: "",
      heureretour: "",
      minuteretour: "",
      prix: "",
      carage:"",
      Userid: this.tokenStorage.getUser().id,
      agence:this.tokenStorage.getUser().Agenceid
  
    }

   this.succesmaintenance();
   

     },
  
  error => {console.log(error);
   this.IsFormNonValid=true;
 });
}

savekilometrage(kilometrage :Kilometrage) {
  this.kilometrage.voitureImmatricule=this.voiture.immatricule;
  this.kilometrageService.add(this.kilometrage)
  .subscribe(data =>{ 
    console.log(data);
   this.succeskilometrage();
   

     },
  
  error => {console.log(error);
   this.IsFormNonValid=true;
 });
}
savecarburant (carburant :Carburant) {
  this.carburant.voitureImmatricule=this.voiture.immatricule;
  this.carburantService.add(this.carburant)
  .subscribe(data =>{ 
    console.log(data);
   this.succescarburant();
   

     },
  
  error => {console.log(error);
   this.IsFormNonValid=true;
 });
}

savediagnostique(diagnostique :Diagnostiques) {
  this.diagnostique.voitureImmatricule=this.voiture.immatricule;
  this.diagnostiquesService.add(this.diagnostique)
  .subscribe(data =>{ 
    console.log(data);
    diagnostique={
      id: "",
      voitureImmatricule: "",
      prix: "",
      description: "",
      carage: "",
      date: null, 
      Userid: this.tokenStorage.getUser().id,
      agence:this.tokenStorage.getUser().Agenceid
    }
   this.succesdiagnostique();
   

     },
  
  error => {console.log(error);
   this.IsFormNonValid=true;
 });
}

savetraqueur(traqueur :Traqueurs) {
  this.traqueur.voitureImmatricule=this.voiture.immatricule;
  this.traqueursService.add(this.traqueur)
  .subscribe(data =>{ 
    console.log(data);
    traqueur={
      id: "",
      voitureImmatricule: "",
      identifiant: "",
      fournisseur: "",
      Userid: this.tokenStorage.getUser().id,
      agence:this.tokenStorage.getUser().Agenceid
    }
   this.succestraqueur();
   

     },
  
  error => {console.log(error);
   this.IsFormNonValid=true;
 });
}

savevistetechnique(vistetechnique :Vistetechnique) {
  this.vistetechnique.voitureImmatricule=this.voiture.immatricule;
  this.vistetechniqueService.add(this.vistetechnique)
  .subscribe(data =>{ 
    console.log(data);
   this.succesvistetechnique();
   

     },
  
  error => {console.log(error);
   this.IsFormNonValid=true;
 });
}

 public NonDisponible(voiture:Voiture,id:String){
  return this.service.updateNonDisponible(voiture,id).subscribe(()=>{
    location.reload();
    this.getAll();

})
}

public delete(id:String){
   


  swal.fire({
    title: 'Êtes-vous sûr?',
    text: "Vous ne pourrez pas revenir en arrière!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Oui, supprimé!'
  }).then((result) => {
  
    if (result.value) {
      return this.service.supprimer(id).subscribe(data=>{
        swal.fire(
          'Effacé!',
          'Votre voiture a été supprimé.',
          'success'
        ).then((r) => {
          location.reload();
        })
      })
      

    }
   // location.reload();  

  
})
}


public update(voiture){
  this.service.updateVoiture(this.voiture.id, this.voiture)
  .subscribe(
    response => {
      console.log(response);
      this.succes();
      
    },
    error => {
      console.log(error);
    });
}

succes(): void{
  swal.fire(
    'Voiture!',
    'votre voiture est Modifier!',
    'success'
  )
}

succesmaintenance(): void{
  swal.fire(
    'maintenance!',
    'votre maintenance est ajouter!',
    'success'
  ).then((r) => {
    location.reload();
  })
}

succeskilometrage(): void{
  swal.fire(
    'kilometrage!',
    'votre kilometrage est ajouter!',
    'success'
  ).then((r) => {
    location.reload();
  })
}

succescarburant(): void{
  swal.fire(
    'carburant!',
    'votre carburant est ajouter!',
    'success'
  ).then((r) => {
    location.reload();
  })
}

succesdiagnostique(): void{
  swal.fire(
    'diagnostique!',
    'votre diagnostique est ajouter!',
    'success'
  ).then((r) => {
    location.reload();
  })
}

succestraqueur(): void{
  swal.fire(
    'traqueur!',
    'votre traqueur est ajouter!',
    'success'
  ).then((r) => {
    location.reload();
  })
}

succesvistetechnique(): void{
  swal.fire(
    'vistetechnique!',
    'votre vistetechnique est ajouter!',
    'success'
  ).then((r) => {
    location.reload();
  })
}
  // js

  private loadScripts() {
    this.dynamicScriptLoader.load('c10','c6','c2','c4','c1','c16').then(data => {
    }).catch(error => console.log(error));
  }

}
