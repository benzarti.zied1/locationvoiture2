import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContratsimpleComponent } from './contratsimple.component';

describe('ContratsimpleComponent', () => {
  let component: ContratsimpleComponent;
  let fixture: ComponentFixture<ContratsimpleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContratsimpleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContratsimpleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
