import { Component, OnInit } from '@angular/core';
import { DynamicFileAdminServiceService } from 'src/app/dynamic-file-service.service';
import { ClientService} from '../../services/client.service';
import {Client} from '../../interface/client';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TokenStorageService } from '../../services/token-storage.service';
import swal from 'sweetalert2';
import { CommonModule } from "@angular/common";
import { OptionService} from '../../services/option.service';
import {Option} from '../../interface/option';

import { ContratService} from '../../services/contrat.service';
import {Contrat} from '../../interface/contrat';

import { VoitureService} from '../../services/voiture.service';
import {Voiture} from '../../interface/voiture';
import {Contratoption} from '../../interface/contratoption';
import {Contratvoiture} from '../../interface/contratvoiture';

import { ReservationService} from '../../services/reservation.service';
import { Reservation} from '../../interface/reservation';


import { TarificationService} from '../../services/tarification.service';
import {Tarification} from '../../interface/tarification';

@Component({
  selector: 'app-contratsimple',
  templateUrl: './contratsimple.component.html',
  styleUrls: ['./contratsimple.component.css']
})
export class ContratsimpleComponent implements OnInit {

  f: FormGroup;
  reservation: Reservation= {
    id: "",
    voitureImmatricule: "",
    dateReservation: null,
    Periodede: null,
    Periodea: null,
    heurePeriodede: "",
    minutePeriodede: "",
    heurePeriodea: "",
    minutePeriodea: "",
    client: "",
    Userid: this.tokenStorage.getUser().id,
    agence:this.tokenStorage.getUser().Agenceid

  }
  reservations:Reservation[]=[];

  IsFormNonValid=false;
  clients:Client[];
  Contratoption:Contratoption={
    contratId: "",
    optionId: ""
  };
  voitures:Voiture[]=[];
  contrat:Contrat={
    id: "",
    periodede: null,
    periodea: null,
    date: null,
    commentaire: "",
    acompte: "",
    prixvoiture: "",
    prixoption:"",
    prixtotal: "",
    client: "",
    paye:"",
    Userid: this.tokenStorage.getUser().id,
    agence: this.tokenStorage.getUser().Agenceid,
    optionId: []

  }

  client:Client={
        id:"",
        nom: "",
        prenom: "",
        cin:"",
        datenaissance:null,
        ville:"",
        codepostale:"",
        tel1: "",
        tel2: "",
        email: "",
        description: "",
        date:null,
        active:"1",
        Userid:this.tokenStorage.getUser().id,
        agence: this.tokenStorage.getUser().Agenceid
  }
  options:Option[]=[];
  listeoption:string[]=[];
option : Option= {
    id: "",
    nom: "",
    prix: "",
    Userid: this.tokenStorage.getUser().id,
    agence:this.tokenStorage.getUser().Agenceid

}

voiture: Voiture={
  id: "",
  immatricule: "",
  annee: "",
  marque: "",
  modele: "",
  categoire: "",
  boite: "",
  clim: "",
  nbporte: 0,
  nbpersonne: 0,
  consommation: "",
  image: "",
  assurance: "",
  datededebutassurance: null,
  datefinassurance: null,
  prixachat: "",
  dateachat: null,
  datemiseencerculation: null,
  disponible: 1,
  Userid: this.tokenStorage.getUser().id,
  Agence: this.tokenStorage.getUser().Agenceid
}
datas:Contratvoiture[]=[];

data={
  immatricule:"",
  datedebut: null,
  datefin:null,
  nbjour:0,
  prix:0
}
prixacompte="";
prixoption=0;
prixoptions=[];
selectedValue:number;
counter = Array;

tprix =[];
tnbjour=[];
prixvoiture=0;
prixtotal=0;
ok=false;
nbjour=0;
prix="";

tarifications:Tarification[]=[];
tarification : Tarification= {
    id: "",
    categoire: "",
    prix: "",
    kilometrage:"",
    Userid: this.tokenStorage.getUser().id,
    agence:this.tokenStorage.getUser().Agenceid

}

  constructor(private tarificationService:TarificationService,private reservationService: ReservationService,private contartservice:ContratService,private voitureservice: VoitureService,private optionService: OptionService,private clientservice:ClientService,private tokenStorage: TokenStorageService,private dynamicScriptLoader: DynamicFileAdminServiceService) { }


  ngOnInit() {
     this.loadScripts();
     this.getAlloption();
     this.getAllReservation();
     this.getAllTarification();
  }
  getAllTarification(){
    this.tarificationService.all().subscribe(data=>{          
      this.tarifications=data; 
      console.log(this.tarifications);  
   })
  }
  getAllReservation(){
    this.reservationService.all().subscribe(data=>{          
      this.reservations=data; 
   })
  }
  idreservation(event) {
    console.log(event.target.value);
    this.getOneReservation(event.target.value);
    this.ok=true;
     //this.datedeb=new Date(this.);
    //this.datefin=new Date(this.data.datefin);


  }

  getOneReservation(id){
    this.reservationService.getReservationById(id).subscribe(data=>{
      
      this.reservation=data;
      this.reservation.client=data['ClientId'];
      console.log(this.reservation); 
      this.calculerjour(this.reservation);

      this.getOneVoiture(this.reservation.voitureImmatricule);


   })
  }
calculerjour(reservation){
  var datede =new Date(this.reservation.Periodede);
  var datefin=new Date(this.reservation.Periodea);

  console.log(this.reservation.Periodede);
  var nb =  datefin.getTime() - datede.getTime();
  this.nbjour= nb / (1000 * 3600 * 24);
  console.log(this.nbjour);
  console.log(this.tarifications.length);
 
}

getOneVoiture(id){
  this.voitureservice.getVoitureById(id).subscribe(data=>{
    
    this.voiture=data;   
    console.log(this.voiture); 
    for (let t of this.tarifications) {
      // "4", "5", "6"
     if(this.voiture.categoire == t.categoire){
       console.log(t.categoire);
       this.prix=t.prix;
       this.prixvoiture=this.nbjour * parseInt(t.prix);
     }
 }
 })
}

save(f){
 
 
  //console.log(this.contrat);
  
  console.log(this.listeoption);
  f.Userid= this.tokenStorage.getUser().id;
  f.agence=this.tokenStorage.getUser().Agenceid;
  f.prixoption=this.prixoption;
  f.optionId=this.listeoption;
  f.prixvoiture=this.prixvoiture;
  f.prixtotal=this.prixtotal;
  f.client=this.reservation.client;
  f.prix0=this.prix;
  f.datedebut0=this.reservation.Periodede;
  f.datefin0=this.reservation.Periodea;
  f.immatricule0=this.reservation.voitureImmatricule;
  f.nbjour0=this.nbjour;
  //console.log(f);
  
  this.contartservice.add(f)
  .subscribe(data =>{ 
    console.log(data);
   this.succes();
   

     },
  
  error => {console.log(error);
   this.IsFormNonValid=true;
 });
 

}
calcultotal(){
  this.prixtotal=0;
  this.prixtotal=this.prixtotal+this.prixvoiture+this.prixoption;
  console.log(this.prixoption);
}



calculoption(){
  this.prixoption=0;
  for (let i in this.listeoption) {
    console.log(this.listeoption[i]);
    var id =this.listeoption[i];
    this.optionService.getOptionById(id).subscribe(data=>{
    
      this.prixoptions.push(data.prix);   
      this.prixoption=this.prixoption+parseInt(data.prix);
      //this.prixtotal=this.prixtotal+this.prixoption;
      this.calcultotal();

   });
  }

   console.log(this.prixoptions);
    //this.getOneOption(this.listeoption[i]);
  
    
    //this.prixoptions.push(this.option.prix)
    //console.log(this.prixoptions);
    /*var y= "500";
    var z ="200";
    var x=parseInt(y)+parseInt(z);
    console.log(x);
    console.log(this.option.prix);
    this.prixoption=this.prixoption +  parseInt(this.option.prix);
    */
  
}

getOneOption(id){
  this.optionService.getOptionById(id).subscribe(data=>{
    
    this.option=data;   
 })
}

 
 

 
  getAlloption(){
    this.optionService.all().subscribe(data=>{          
      this.options=data; 
      console.log(this.options);  
   })
  }

  succesclient(): void{
    swal.fire(
      'client!',
      'votre Client est ajouter!',
      'success'
    ).then((r) => {
      location.reload();
    })
  }

  succes(): void{
    swal.fire(
      'contrat!',
      'votre Contrat est ajouter!',
      'success'
    ).then((r) => {
      location.reload();
    })
  }
  
  calvoiture(f){
    this.prixvoiture=0;
    this.tprix=[];
    this.tnbjour=[];
    for (var key in f) 
    {
    if(key.indexOf('prix')> -1)
    {
        
         this.tprix.push(f[key]);
    }
    if(key.indexOf('nbjour')> -1)
    {
        
         this.tnbjour.push(f[key]);
    }
   }
   for(var i in this.tprix){
     this.prixvoiture=this.prixvoiture+(parseInt(this.tprix[i])*parseInt(this.tnbjour[i]));
   }
   this.calcultotal();
   
    }

    change(event) {console.log(event.target.value);}

  
 

  // js
//'c6','c2','c4','c1','c16','c12','c13'


  private loadScripts() {
    this.dynamicScriptLoader.load('c10','c6','c2','c4','c1','c16','c12','c13').then(data => {
    }).catch(error => console.log(error));
  }

}
