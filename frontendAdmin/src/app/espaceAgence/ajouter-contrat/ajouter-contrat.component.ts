import { Component, OnInit } from '@angular/core';
import { DynamicFileAdminServiceService } from 'src/app/dynamic-file-service.service';
import { ClientService} from '../../services/client.service';
import {Client} from '../../interface/client';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TokenStorageService } from '../../services/token-storage.service';
import swal from 'sweetalert2';
import { CommonModule } from "@angular/common";
import { OptionService} from '../../services/option.service';
import {Option} from '../../interface/option';

import { ContratService} from '../../services/contrat.service';
import {Contrat} from '../../interface/contrat';

import { VoitureService} from '../../services/voiture.service';
import {Voiture} from '../../interface/voiture';
import {Contratoption} from '../../interface/contratoption';
import {Contratvoiture} from '../../interface/contratvoiture';
import { TarificationService} from '../../services/tarification.service';
import {Tarification} from '../../interface/tarification';



@Component({
  selector: 'app-ajouter-contrat',
  templateUrl: './ajouter-contrat.component.html',
  styleUrls: ['./ajouter-contrat.component.css']
})
export class AjouterContratComponent implements OnInit {
  prixi:Array<number>=[];
  nbrjouri:Array<number>=[];
  tdatedebut:Array<Date>=[];
  tdatefin:Array<Date>=[];
  nbjour:Array<number>=[];
  timmatricule:Array<string>=[];
  tprixvoiture:Array<string>=[];
  tcategoire:Array<string>=[];

  f: FormGroup;
  IsFormNonValid=false;
  clients:Client[];
  Contratoption:Contratoption={
    contratId: "",
    optionId: ""
  };
  voitures:Voiture[]=[];
  contrat:Contrat={
    id: "",
    periodede: null,
    periodea: null,
    date: null,
    commentaire: "",
    acompte: "",
    prixvoiture: "",
    prixoption:"",
    prixtotal: "",
    client: "",
    paye:"",
    Userid: this.tokenStorage.getUser().id,
    agence: this.tokenStorage.getUser().Agenceid,
    optionId: []

  }

  client:Client={
        id:"",
        nom: "",
        prenom: "",
        cin:"",
        datenaissance:null,
        ville:"",
        codepostale:"",
        tel1: "",
        tel2: "",
        email: "",
        description: "",
        date:null,
        active:"1",
        Userid:this.tokenStorage.getUser().id,
        agence: this.tokenStorage.getUser().Agenceid
  }
  options:Option[]=[];
  listeoption:string[]=[];
option : Option= {
    id: "",
    nom: "",
    prix: "",
    Userid: this.tokenStorage.getUser().id,
    agence:this.tokenStorage.getUser().Agenceid

}

voiture: Voiture={
  id: "",
  immatricule: "",
  annee: "",
  marque: "",
  modele: "",
  categoire: "",
  boite: "",
  clim: "",
  nbporte: 0,
  nbpersonne: 0,
  consommation: "",
  image: "",
  assurance: "",
  datededebutassurance: null,
  datefinassurance: null,
  prixachat: "",
  dateachat: null,
  datemiseencerculation: null,
  disponible: 1,
  Userid: this.tokenStorage.getUser().id,
  Agence: this.tokenStorage.getUser().Agenceid
}
datas:Contratvoiture[]=[];
tarifications:Tarification[]=[];

data={
  immatricule:"",
  datedebut: null,
  datefin:null,
  nbjour:0,
  prix:0
}
prixacompte="";
prixoption=0;
prixoptions=[];
selectedValue:number;
counter = Array;
datedebut=null;
tprix =[];
tnbjour=[];
prixvoiture=0;
prixtotal=0;
c=0;
  constructor(private tarificationService:TarificationService,private contartservice:ContratService,private voitureservice: VoitureService,private optionService: OptionService,private clientservice:ClientService,private tokenStorage: TokenStorageService,private dynamicScriptLoader: DynamicFileAdminServiceService) { }


  ngOnInit() {
     this.loadScripts();
     this.getAllclient(); 
     this.getAlloption();
     this.getAllVoiture();
     this.getAlltarification();
  }
 
save(f){
 
 
  //console.log(this.contrat);
  
  console.log(this.listeoption);
  f.Userid= this.tokenStorage.getUser().id;
  f.agence=this.tokenStorage.getUser().Agenceid;
  f.prixoption=this.prixoption;
  f.optionId=this.listeoption;
  f.prixvoiture=this.prixvoiture;
  f.prixtotal=this.prixtotal;
  this.contartservice.add(f)
  .subscribe(data =>{ 
    console.log(data);
   this.succes();
   

     },
  
  error => {console.log(error);
   this.IsFormNonValid=true;
 });
 

}

getAlltarification(){
  this.tarificationService.all().subscribe(data=>{          
    this.tarifications=data; 
    console.log(this.tarifications);  
 })
}

calcultotal(){
  this.prixtotal=0;
  this.prixtotal=this.prixtotal+this.prixvoiture+this.prixoption;
  console.log(this.prixoption);
}

calculacompte(){
  this.prixacompte=this.contrat.acompte;
  this.calcultotal();
}

calculoption(){
  this.prixoption=0;
  for (let i in this.listeoption) {
    console.log(this.listeoption[i]);
    var id =this.listeoption[i];
    this.optionService.getOptionById(id).subscribe(data=>{
    
      this.prixoptions.push(data.prix);   
      this.prixoption=this.prixoption+parseInt(data.prix);
      //this.prixtotal=this.prixtotal+this.prixoption;
      this.calcultotal();

   });
  }

   console.log(this.prixoptions);
    //this.getOneOption(this.listeoption[i]);
  
    
    //this.prixoptions.push(this.option.prix)
    //console.log(this.prixoptions);
    /*var y= "500";
    var z ="200";
    var x=parseInt(y)+parseInt(z);
    console.log(x);
    console.log(this.option.prix);
    this.prixoption=this.prixoption +  parseInt(this.option.prix);
    */
  
}

getOneOption(id){
  this.optionService.getOptionById(id).subscribe(data=>{
    
    this.option=data;   
 })
}

  getAllVoiture(){
    this.voitureservice.allDispo().subscribe(data=>{          
      this.voitures=data; 
      console.log(this.voitures);   
   })
 }
  saveclient(client :Client) {
    this.clientservice.addClient(this.client)
    
    .subscribe(data =>{ 
      console.log(data);
     this.succesclient();
     
  
       },
    
    error => {console.log(error);
     this.IsFormNonValid=true;
   });
  }

  getAllclient(){
    this.clientservice.allClient().subscribe(data=>{          
      this.clients=data; 
      console.log(this.clients);  
   })
  }
  getAlloption(){
    this.optionService.all().subscribe(data=>{          
      this.options=data; 
      console.log(this.options);  
   })
  }

  succesclient(): void{
    swal.fire(
      'client!',
      'votre Client est ajouter!',
      'success'
    ).then((r) => {
      location.reload();
    })
  }

  succes(): void{
    swal.fire(
      'contrat!',
      'votre Contrat est ajouter!',
      'success'
    ).then((r) => {
      location.reload();
    })
  }
  
  calvoiture(f){
    this.prixvoiture=0;
    this.tprix=[];
    this.tnbjour=[];
    for (var key in f) 
    {
    if(key.indexOf('prix')> -1)
    {
        
         this.tprix.push(f[key]);
    }
    if(key.indexOf('nbjour')> -1)
    {
        
         this.tnbjour.push(f[key]);
    }
   }
   for(var i in this.tprix){
     this.prixvoiture=this.prixvoiture+(parseInt(this.tprix[i])*parseInt(this.tnbjour[i]));
   }
   this.calcultotal();
   console.log(this.tnbjour);
   console.log(this.tprix);
   
    }
    change(event) {console.log("event "+event.target.value);}


    calculernbjour(){
      this.nbjour=[];

      for ( var i=0;i<this.selectedValue;i++){
        var datedeb=new Date(this.tdatedebut[i]);
      var datefin=new Date(this.tdatefin[i]);
     var nb = datefin.getTime() - datedeb.getTime();
    this.nbjour[i]= nb / (1000 * 3600 * 24);
  
    
      }

    }
  

   

  calculdate(){
    //var nb = this.data["datedebut"].getTime() - this.data["datefin"].getTime();
    //const d = this.f.value['datedebut'];
var datedeb=new Date(this.datedebut);
var datefin=new Date(this.data.datefin);
var nb = datefin.getTime() - datedeb.getTime();
var nbjour= nb / (1000 * 3600 * 24);
this.data.nbjour=nbjour;
//this.voiturecontrat.push(this.data);


//console.log(this.datas[0].datedebut);
    //this.data["nbjour"]=nb;
    //console.log(nb);
    console.log(this.datedebut);
   // console.log(this.counter.length);
   // console.log(this.selectedValue);
  }
  calc(){
   // console.log(d);
    var date1 = new Date("12/12/2020");
    var date2 = new Date("12/12/2021");
     // différence des heures
    var time_diff = date2.getTime() - date1.getTime();
     // différence de jours
    var days_Diff = time_diff / (1000 * 3600 * 24);
    // afficher la différence
    alert(days_Diff);
  }

  // js
//'c6','c2','c4','c1','c16','c12','c13'


  private loadScripts() {
    this.dynamicScriptLoader.load('c10','c6','c2','c4','c1','c16','c12','c13').then(data => {
    }).catch(error => console.log(error));
  }
}
