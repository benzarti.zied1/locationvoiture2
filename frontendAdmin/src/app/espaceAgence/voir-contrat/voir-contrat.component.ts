import { Component, OnInit , ViewChild, ElementRef} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ContratService} from '../../services/contrat.service';
import {Contrat} from '../../interface/contrat';
import { TokenStorageService } from '../../services/token-storage.service';
import { AgenceService} from '../../services/agence.service';
import {Agence} from '../../interface/agence';
import { ClientService} from '../../services/client.service';
import {Client} from '../../interface/client';
import * as jsPDF from 'jspdf'

import html2canvas from 'html2canvas'; 



@Component({
  selector: 'app-voir-contrat',
  templateUrl: './voir-contrat.component.html',
  styleUrls: ['./voir-contrat.component.css']
})
export class VoirContratComponent implements OnInit {
  @ViewChild('pdfTable', {static: false}) pdfTable: ElementRef;
  contrat:Contrat={
    id: "",
    periodede: null,
    periodea: null,
    date: null,
    commentaire: "",
    acompte: "",
    prixvoiture: "",
    prixoption:"",
    prixtotal: "",
    client: "",
    paye:"",
    Userid: this.tokenStorage.getUser().id,
    agence: this.tokenStorage.getUser().Agenceid,
    optionId: []

  }

  agence: Agence = {
    email: '',
    username: '',
    password:'',
    active:0,
    nom:'',
    tel:'',
    adressepostale:'',
    ville:'',
    tva:'',
    compteboncaire:'',
    image:''
  };
  client:Client={
    id:"",
    nom: "",
    prenom: "",
    cin:"",
    datenaissance:null,
    ville:"",
    codepostale:"",
    tel1: "",
    tel2: "",
    email: "",
    description: "",
    date:null,
    active:"1",
    Userid:this.tokenStorage.getUser().id,
    agence: this.tokenStorage.getUser().Agenceid
}
voitures={
  immatricule:"",
  datedebut: null,
  datefin:null,
  nbjour:0,
  prix:0
}
id;
email;
  constructor(private clientservice:ClientService,private service:AgenceService,private tokenStorage: TokenStorageService,private contartservice:ContratService,private activeRoute: ActivatedRoute,private router:Router) { }

  ngOnInit(): void {
    this.activeRoute.queryParams
    .subscribe((params) => 
      {this.id=params["id"];
       console.log(this.id);
        
  });
  this.email=this.tokenStorage.getUser().email;
  this.getOne(this.id);
  this.getOneAgence(this.tokenStorage.getUser().Agenceid)

  }

  getOne(id){
    this.contartservice.getContratById(id).subscribe(data=>{
      
      this.contrat=data;  
      console.log(this.contrat);
      console.log(data['ClientId']);
      this.getOneclient(data['ClientId']);
      this.getVoiture(data.id);

   })
  }

  getOneAgence(id){
    this.service.getAgenceById(id).subscribe(data=>{ 
      this.agence=data;  
      console.log(this.agence);
   })
  }

  getOneclient(id){
    this.clientservice.getclientById(id).subscribe(data=>{
      
      this.client=data;  
      console.log(this.client); 
   })
  }

  getVoiture(id){
    this.contartservice.getVoiture(id).subscribe(data=>{
      this.voitures=data;
      console.log(this.voitures);
    })
  }


  Screen()
  {  
      var data = document.getElementById('content');  
      html2canvas(data).then(canvas => {  
          // Few necessary setting options  
          var imgWidth = 600;   
          var pageHeight = 600;    
          var imgHeight = canvas.height * imgWidth / canvas.width;  
          var heightLeft = imgHeight;  
  
          const contentDataURL = canvas.toDataURL('image/png')  
          let pdf = new jsPDF('p', 'pt', 'a4'); // A4 size page of PDF  
          var position = 0;  
          pdf.addImage(contentDataURL, 'PNG',0,0,canvas.width, canvas.height)  
          pdf.save('MYPdf.pdf'); // Generated PDF   
      });  
  }


}
