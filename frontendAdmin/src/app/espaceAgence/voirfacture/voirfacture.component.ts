import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TokenStorageService } from '../../services/token-storage.service';
import { AgenceService} from '../../services/agence.service';
import {Agence} from '../../interface/agence';
import { ClientService} from '../../services/client.service';
import {Client} from '../../interface/client';
import { FactureService} from '../../services/facture.service';
import {Facture} from '../../interface/facture';
import { ArticleService} from '../../services/article.service';
import {Article} from '../../interface/article';

@Component({
  selector: 'app-voirfacture',
  templateUrl: './voirfacture.component.html',
  styleUrls: ['./voirfacture.component.css']
})
export class VoirfactureComponent implements OnInit {
id;
agence: Agence = {
  email: '',
  username: '',
  password:'',
  active:0,
  nom:'',
  tel:'',
  adressepostale:'',
  ville:'',
  tva:'',
  compteboncaire:'',
  image:''
};
client:Client={
  id:"",
  nom: "",
  prenom: "",
  cin:"",
  datenaissance:null,
  ville:"",
  codepostale:"",
  tel1: "",
  tel2: "",
  email: "",
  description: "",
  date:null,
  active:"1",
  Userid:this.tokenStorage.getUser().id,
  agence: this.tokenStorage.getUser().Agenceid
}
facture : Facture={
  id: "",
  date: "",
  delai : "",
  Prixtotal: "",
  paye: "",
  client:"",
  Userid: this.tokenStorage.getUser().id,
  agence: this.tokenStorage.getUser().Agenceid
}

articles:Article[]=[];
  article: Article={
    id: "",
    nom: "",
    prix: "",
    tva: "", 
    prixttc:"",
    cout:"",
    Userid: this.tokenStorage.getUser().id,
    agence:this.tokenStorage.getUser().Agenceid
  }

  facturearticle={
    contart:"",
    article:"",
    Qte:""
  }
  farticle=[{facture:"",
    article:"",
    Qte:""
  }];

  t=[];

  f={
    nom:"",
    prix:"",
    tva:"",
    prixttc:"",
    qte:"",
    prixtotal:0
  }
  constructor(private articleservice: ArticleService,private factureservice:FactureService,private clientservice:ClientService,private service:AgenceService,private tokenStorage: TokenStorageService,private activeRoute: ActivatedRoute,private router:Router) { }

  ngOnInit(): void {
    this.activeRoute.queryParams
    .subscribe((params) => 
      {this.id=params["id"];
       console.log(this.id);
        
  });
  this.getOneAgence(this.tokenStorage.getUser().Agenceid)
  this.getOne(this.id);
  
  this.getAllfacturearticle(this.id);
  }

  getOne(id){
    this.factureservice.getFactureById(id).subscribe(data=>{
      
      this.facture=data;  
      console.log(this.facture);
      this.facture.client=data['ClientId'];
      console.log(data['ClientId']);
      this.getOneclient(data['ClientId']);
      //this.getVoiture(data.id);
;
   })
  }

  getOneclient(id){
    this.clientservice.getclientById(id).subscribe(data=>{
      
      this.client=data;  
      
      //console.log(this.client); 
   })
  }

  getOneAgence(id){
    this.service.getAgenceById(id).subscribe(data=>{ 
      this.agence=data;  
      console.log(this.agence);
   })
  }


  getAllarticle(){
    this.articleservice.all().subscribe(data=>{          
      this.articles=data; 
      //console.log(this.articles);
   })
  }

  getAllfacturearticle(id){
    this.getAllarticle();
    this.factureservice.getFacturearticle(id).subscribe(data=>{ 
      this.farticle=data;  
      //console.log(this.farticle);
      this.calculer();

      
   })
  }

calculer(){
 //this.t=[];
  for (var i=0; i<this.farticle.length;i++){
    for (var j=0; j<this.articles.length;j++){
      console.log(this.farticle[i].article);
      console.log(this.articles[j].id);
      if(this.farticle[i].facture == this.id && this.farticle[i].article == this.articles[j].id){
        
this.f.nom=this.articles[j].nom;
this.f.prix=this.articles[j].prix;
this.f.prixttc=this.articles[j].prixttc;
this.f.tva=this.articles[j].tva;
this.f.qte=this.farticle[i].Qte;
this.f.prixtotal=parseInt(this.articles[j].prixttc)*parseInt(this.farticle[i].Qte);

//console.log(this.farticle[i].facture);
//console.log(this.f);
this.t.push(this.f);
this.f={
  nom:"",
  prix:"",
  tva:"",
  prixttc:"",
  qte:"",
  prixtotal:0
}
//console.log(this.t);
      }
   
    }
  }
  
}

}
