import { Component, OnInit } from '@angular/core';
import { AgenceService} from '../../services/agence.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-password',
  templateUrl: './password.component.html',
  styleUrls: ['./password.component.css']
})
export class PasswordComponent implements OnInit {
  emailExist;
  constructor(private service:AgenceService,private router:Router) { }

  ngOnInit(): void {
  }

  forgetPassword(utilisateur){
    this.service.getCode(utilisateur.email).subscribe((data)=>{
      console.log(data["message"]);
      if(data["message"])
        this.emailExist=false;
        else{
          this.emailExist=true;
          this.router.navigate(['/RestePassword'], { queryParams: {email: utilisateur.email}});

        }

    })
    console.log(utilisateur.email);

  }

}
