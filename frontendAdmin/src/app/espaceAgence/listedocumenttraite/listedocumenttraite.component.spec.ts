import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListedocumenttraiteComponent } from './listedocumenttraite.component';

describe('ListedocumenttraiteComponent', () => {
  let component: ListedocumenttraiteComponent;
  let fixture: ComponentFixture<ListedocumenttraiteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListedocumenttraiteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListedocumenttraiteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
