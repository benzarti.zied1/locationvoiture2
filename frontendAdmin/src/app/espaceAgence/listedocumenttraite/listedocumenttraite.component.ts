import { Component, OnInit } from '@angular/core';
import { DynamicFileAdminServiceService } from 'src/app/dynamic-file-service.service';
import { FileService } from 'src/app/services/file.service';
import { ClientService} from '../../services/client.service';
import {Client} from '../../interface/client';
import swal from 'sweetalert2';

@Component({
  selector: 'app-listedocumenttraite',
  templateUrl: './listedocumenttraite.component.html',
  styleUrls: ['./listedocumenttraite.component.css']
})
export class ListedocumenttraiteComponent implements OnInit {

  constructor(private clientservice:ClientService,private dynamicScriptLoader: DynamicFileAdminServiceService,private service:FileService) { }
  files=[];
  clients:Client[];
  InvalidFrom;
  ngOnInit(): void {
    this.getAll();
    this.getAllclient();
    this.loadScripts();

  }
  getAllclient(){
    this.clientservice.allClient().subscribe(data=>{          
      this.clients=data; 
      console.log(this.clients);  
   })
  }
  public getAll(){
   
    return this.service.searchFileTraite().subscribe(data=>{  
      console.log(data);
      this.files=data;
     })
  }
 

  public delete(id){
     
  
  
    swal.fire({
      title: 'Êtes-vous sûr?',
      text: "Vous ne pourrez pas revenir en arrière!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Oui, supprimé!'
    }).then((result) => {
    
      if (result.value) {
        return this.service.deleteTraite(id).subscribe(data=>{
          swal.fire(
            'Effacé!',
            'Votre Document a été supprimé.',
            'success'
          ).then((r) => {
            location.reload();
          })
        })
        
  
      }
     // location.reload();  
  
    
  })
  }
  update(f){
    this.service.update(f)
    .subscribe(data =>{ 
    // this.succes();
    location.reload();
  },error=>{
    this.InvalidFrom=error["error"]["message"]; 
  
  })
  }

  private loadScripts() {
    this.dynamicScriptLoader.load('c10','c6','c2','c4','c1').then(data => {
    }).catch(error => console.log(error));
  }
}
