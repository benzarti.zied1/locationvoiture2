import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TraqueursComponent } from './traqueurs.component';

describe('TraqueursComponent', () => {
  let component: TraqueursComponent;
  let fixture: ComponentFixture<TraqueursComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TraqueursComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TraqueursComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
