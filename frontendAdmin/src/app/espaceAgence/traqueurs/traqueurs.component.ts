import { Component, OnInit } from '@angular/core';
import { DynamicFileAdminServiceService } from 'src/app/dynamic-file-service.service';
import { VoitureService} from '../../services/voiture.service';
import { TraqueursService} from '../../services/traqueurs.service';
import {Voiture} from '../../interface/voiture';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TokenStorageService } from '../../services/token-storage.service';
import swal from 'sweetalert2';
import { CommonModule } from "@angular/common";
import { Traqueurs} from '../../interface/traqueurs';


@Component({
  selector: 'app-traqueurs',
  templateUrl: './traqueurs.component.html',
  styleUrls: ['./traqueurs.component.css']
})
export class TraqueursComponent implements OnInit {
  voitures:Voiture[]=[];
  traqueurs:Traqueurs[]=[];
  IsFormNonValid=false;
  traqueur:Traqueurs={
    id: "",
    voitureImmatricule: "",
    identifiant: "",
    fournisseur: "",
    Userid: this.tokenStorage.getUser().id,
    agence:this.tokenStorage.getUser().Agenceid
  }
  id="";
  constructor(private traqueursService: TraqueursService,private dynamicScriptLoader: DynamicFileAdminServiceService,private tokenStorage: TokenStorageService,private service: VoitureService) { }


  ngOnInit() {
   
    
     this.loadScripts();
     this.getAllVoiture();
     this.getAll();
  }
  getAllVoiture(){
    this.service.allDispo().subscribe(data=>{          
      this.voitures=data; 
      console.log(this.voitures);   
   })
 }

 save(traqueur :Traqueurs) {
  this.traqueursService.add(this.traqueur)
  .subscribe(data =>{ 
    console.log(data);
    traqueur={
      id: "",
      voitureImmatricule: "",
      identifiant: "",
      fournisseur: "",
      Userid: this.tokenStorage.getUser().id,
      agence:this.tokenStorage.getUser().Agenceid
    }
   this.succes();
   

     },
  
  error => {console.log(error);
   this.IsFormNonValid=true;
 });
}

getAll(){
  this.traqueursService.all().subscribe(data=>{          
    this.traqueurs=data; 
 })
}

public delete(id:String){
   


  swal.fire({
    title: 'Êtes-vous sûr?',
    text: "Vous ne pourrez pas revenir en arrière!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Oui, supprimé!'
  }).then((result) => {
  
    if (result.value) {
      return this.traqueursService.supprimer(id).subscribe(data=>{
        swal.fire(
          'Effacé!',
          'Votre traqueurs a été supprimé.',
          'success'
        ).then((r) => {
          location.reload();
        })
      })
      

    }
   // location.reload();  

  
})
}

getOne(id){
  this.traqueursService.getTraqueursById(id).subscribe(data=>{
    
    this.traqueur=data;   
 })
}

public update(traqueur){
  this.traqueursService.updateTraqueurs(this.traqueur.id, this.traqueur)
  .subscribe(
    response => {
      console.log(response);
      this.modifier();
      
    },
    error => {
      console.log(error);
    });
}

initi():void{
  this.traqueur={
    id: "",
    voitureImmatricule: "",
    identifiant: "",
    fournisseur: "",
    Userid: this.tokenStorage.getUser().id,
    agence:this.tokenStorage.getUser().Agenceid
  }
 }

succes(): void{
  swal.fire(
    'Traqueurs!',
    'votre Traqueurs est ajouter!',
    'success'
  ).then((r) => {
    location.reload();
  })
}

modifier(): void{
  swal.fire(
    'Traqueurs!',
    'votre Traqueurs est modifier!',
    'success'
  ).then((r) => {
    location.reload();
  })
}
  // js

  private loadScripts() {
    this.dynamicScriptLoader.load('c10','c6','c2','c4','c1','c16').then(data => {
    }).catch(error => console.log(error));
  }

}
