import { Component, OnInit } from '@angular/core';
import { DynamicFileAdminServiceService } from 'src/app/dynamic-file-service.service';
import { AgenceService} from '../../services/agence.service';
import {Agence} from '../../interface/agence';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
//import swal from 'sweetalert2';
import swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'app-listeagence',
  templateUrl: './listeagence.component.html',
  styleUrls: ['./listeagence.component.css']
})
export class ListeagenceComponent implements OnInit {
  user: Agence = {
    email: '',
    username: '',
    password:'',
    active:0,
    nom:'',
    tel:'',
    adressepostale:'',
    ville:'',
    tva:'',
    compteboncaire:'',
    image:''
  };
  agences:any[]=[];
  users:any[]=[];
  IsFormValid=false;
  IsFormNonValid=false;
  constructor(private service:AgenceService,private dynamicScriptLoader: DynamicFileAdminServiceService) { }

  ngOnInit(): void {
    this.loadScripts();
    this.getAllagence();
    this.getAlluser();
  }

  getAllagence(){
    this.service.allagence().subscribe(data=>{
      this.agences=data; 
      console.log(this.agences);   
   })
  }

  getAlluser(){
    this.service.alluser().subscribe(data=>{
      this.users=data;    
      console.log(this.users);
   })
  }


  save(user:Agence) {
    
    console.log(user)
    this.service.add(this.user)
     .subscribe(data =>{ console.log(data);
      this.succes();
      
     
        },
     
     error => {console.log(error);
      this.IsFormNonValid=true;
    });

     

    
   
  }
     
  succes(): void{
    swal.fire(
      'Incription!',
      'votre compte est valide!',
      'success'
    )

  

  }


  public active(agence:Agence,id:String){
    return this.service.updateactive(agence,id).subscribe(()=>{
      location.reload();
  })
  }


  public desactive(agence:Agence,id:String){
    
    return this.service.updatedesactive(agence,id).subscribe(()=>{
      location.reload();
  })
  }


  public delete(id:String){
   


    swal.fire({
      title: 'Êtes-vous sûr?',
      text: "Vous ne pourrez pas revenir en arrière!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Oui, supprimé!'
    }).then((result) => {
    
      if (result.value) {
        return this.service.supprimer(id).subscribe(data=>{
          swal.fire(
            'Effacé!',
            'Votre Agence a été supprimé.',
            'success'
          ).then((r) => {
            location.reload();
          })
        })
        
  
      }
     // location.reload();  
  
    
  })
  }

  private loadScripts() {
    this.dynamicScriptLoader.load('c10','c6','c2','c4','c1').then(data => {
    }).catch(error => console.log(error));
  }
}
