import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListeagenceComponent } from './listeagence.component';

describe('ListeagenceComponent', () => {
  let component: ListeagenceComponent;
  let fixture: ComponentFixture<ListeagenceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListeagenceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListeagenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
