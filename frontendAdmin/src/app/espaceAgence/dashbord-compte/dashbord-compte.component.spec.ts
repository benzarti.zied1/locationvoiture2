import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashbordCompteComponent } from './dashbord-compte.component';

describe('DashbordCompteComponent', () => {
  let component: DashbordCompteComponent;
  let fixture: ComponentFixture<DashbordCompteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashbordCompteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashbordCompteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
