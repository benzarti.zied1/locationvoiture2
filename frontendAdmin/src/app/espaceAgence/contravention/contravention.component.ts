import { Component, OnInit } from '@angular/core';
import { DynamicFileAdminServiceService } from 'src/app/dynamic-file-service.service';
import { VoitureService} from '../../services/voiture.service';
import { ContraventionService} from '../../services/contravention.service';
import {Voiture} from '../../interface/voiture';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TokenStorageService } from '../../services/token-storage.service';
import swal from 'sweetalert2';
import { CommonModule } from "@angular/common";
import { Contravention} from '../../interface/contravention';

import { ClientService} from '../../services/client.service';
import {Client} from '../../interface/client';


@Component({
  selector: 'app-contravention',
  templateUrl: './contravention.component.html',
  styleUrls: ['./contravention.component.css']
})
export class ContraventionComponent implements OnInit {
  voitures:Voiture[]=[];
  clients:Client[]=[];
  contraventions:Contravention[]=[];
  IsFormNonValid=false;
  contravention: Contravention= {
    id: "",
    voitureImmatricule: "",
    dateafraction: null,
    montant: "",
    client: "",
    Userid: this.tokenStorage.getUser().id,
    agence:this.tokenStorage.getUser().Agenceid

  }
  id="";
  constructor(private clientservice: ClientService,private service: VoitureService,private contraventionService: ContraventionService,private tokenStorage: TokenStorageService,private dynamicScriptLoader: DynamicFileAdminServiceService) { }


  ngOnInit() {
   
    
     this.loadScripts();
     this.getAll();
     this.getAllVoiture();
     this.getAllClient();
  }

  getAllVoiture(){
    this.service.allDispo().subscribe(data=>{          
      this.voitures=data; 
      console.log(this.voitures);   
   })
 }

 getAllClient(){
  this.clientservice.allClient().subscribe(data=>{          
    this.clients=data; 
    console.log(this.clients);   
 })
}

  save(contravention :Contravention) {
    //this.contravention.client="20a9638b-94cd-4e49-991e-0b5d57f8c374";
    console.log(contravention);
    this.contraventionService.add(this.contravention)
    .subscribe(data =>{ 
      console.log(data);
     this.succes();
     
  
       },
    
    error => {console.log(error);
     this.IsFormNonValid=true;
   });
  }
  
  getAll(){
    this.contraventionService.all().subscribe(data=>{          
      this.contraventions=data; 
      console.log(this.contraventions);  
   })
  }
  
  public delete(id:String){
     
  
  
    swal.fire({
      title: 'Êtes-vous sûr?',
      text: "Vous ne pourrez pas revenir en arrière!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Oui, supprimé!'
    }).then((result) => {
    
      if (result.value) {
        return this.contraventionService.supprimer(id).subscribe(data=>{
          swal.fire(
            'Effacé!',
            'Votre contravention a été supprimé.',
            'success'
          ).then((r) => {
            location.reload();
          })
        })
        
  
      }
     // location.reload();  
  
    
  })
  }
  
  
  getOne(id){
    this.contraventionService.getContraventionById(id).subscribe(data=>{
      
      this.contravention=data;   
   })
  }
  
  public update(contravention){
    this.contraventionService.updateContravention(this.contravention.id, this.contravention)
    .subscribe(
      response => {
        console.log(response);
        this.modifier();
        
      },
      error => {
        console.log(error);
      });
  }
  
  
  initi():void{
    this.contravention= {
      id: "",
      voitureImmatricule: "",
      dateafraction: null,
      montant: "",
      client: "",
      Userid: this.tokenStorage.getUser().id,
      agence:this.tokenStorage.getUser().Agenceid
  
    }
   }
    // js
  
  
    succes(): void{
      swal.fire(
        'contravention!',
        'votre contravention est ajouter!',
        'success'
      ).then((r) => {
        location.reload();
      })
    }
  
    modifier(): void{
      swal.fire(
        'contravention!',
        'votre contravention est modifier!',
        'success'
      ).then((r) => {
        location.reload();
      })
    }
    
  // js

  private loadScripts() {
    this.dynamicScriptLoader.load('c10','c6','c2','c4','c1','c16').then(data => {
    }).catch(error => console.log(error));
  }

}
