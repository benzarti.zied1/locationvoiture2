import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashborddocumentComponent } from './dashborddocument.component';

describe('DashborddocumentComponent', () => {
  let component: DashborddocumentComponent;
  let fixture: ComponentFixture<DashborddocumentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashborddocumentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashborddocumentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
