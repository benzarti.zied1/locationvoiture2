import { Component, OnInit } from '@angular/core';
import { DynamicFileAdminServiceService } from 'src/app/dynamic-file-service.service';

@Component({
  selector: 'app-listepayment',
  templateUrl: './listepayment.component.html',
  styleUrls: ['./listepayment.component.css']
})
export class ListepaymentComponent implements OnInit {

  constructor(private dynamicScriptLoader: DynamicFileAdminServiceService) { }

  ngOnInit(): void {
    this.loadScripts();

  }


  private loadScripts() {
    this.dynamicScriptLoader.load('c10','c6','c2','c4','c1','c16').then(data => {
    }).catch(error => console.log(error));
  }
}
