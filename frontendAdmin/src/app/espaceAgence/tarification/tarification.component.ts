import { Component, OnInit } from '@angular/core';
import { DynamicFileAdminServiceService } from 'src/app/dynamic-file-service.service';
import { TarificationService} from '../../services/tarification.service';
import {Tarification} from '../../interface/tarification';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TokenStorageService } from '../../services/token-storage.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-tarification',
  templateUrl: './tarification.component.html',
  styleUrls: ['./tarification.component.css']
})
export class TarificationComponent implements OnInit {
  tarifications:Tarification[]=[];
  IsFormNonValid=false;
  tarification : Tarification= {
      id: "",
      categoire: "",
      prix: "",
      kilometrage:"",
      Userid: this.tokenStorage.getUser().id,
      agence:this.tokenStorage.getUser().Agenceid
  
  }
  id="";
  t:Tarification= {
    id: "",
    categoire: "",
    prix: "",
    kilometrage:"",
    Userid: this.tokenStorage.getUser().id,
    agence:this.tokenStorage.getUser().Agenceid

}
message="";
ok=false;
  constructor(private tarificationService:TarificationService,private dynamicScriptLoader: DynamicFileAdminServiceService,private tokenStorage: TokenStorageService) { }

  ngOnInit(): void {
    this.loadScripts();
    this.getAll();
   

  }

  save(tarification :Tarification) {

    console.log(this.tarification);
    this.tarificationService.add(this.tarification)
    .subscribe(data =>{ 
      console.log(data);
     this.succes();
     
  
       },
    
    error => {console.log(error);
     this.IsFormNonValid=true;
   }); 
    
  }



  getverifr(t){
    this.tarificationService.getverifier(t).subscribe(data=>{          
      this.t=data; 
      console.log(this.t);  
   })
  }

  getAll(){
    this.tarificationService.all().subscribe(data=>{          
      this.tarifications=data; 
      console.log(this.tarifications);  
   })
  }
  
  public delete(id:String){
     
  
  
    swal.fire({
      title: 'Êtes-vous sûr?',
      text: "Vous ne pourrez pas revenir en arrière!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Oui, supprimé!'
    }).then((result) => {
    
      if (result.value) {
        return this.tarificationService.supprimer(id).subscribe(data=>{
          swal.fire(
            'Effacé!',
            'Votre tarification a été supprimé.',
            'success'
          ).then((r) => {
            location.reload();
          })
        })
        
  
      }
     // location.reload();  
  
    
  })
  }
  
  
  getOne(id){
    this.tarificationService.gettarificationById(id).subscribe(data=>{
      
      this.tarification=data;   
   })
  }
  
  public update(tarification){
    this.tarificationService.updatetarification(this.tarification.id, this.tarification)
    .subscribe(
      response => {
        console.log(response);
        this.modifier();
        
      },
      error => {
        console.log(error);
      });
  }
  
  
  initi():void{
    this.tarification= {
      id: "",
      categoire: "",
      prix: "",
      kilometrage:"",
      Userid: this.tokenStorage.getUser().id,
      agence:this.tokenStorage.getUser().Agenceid
  
  }
   }
    // js
  
  
    succes(): void{
      swal.fire(
        'tarification!',
        'votre tarification est ajouter!',
        'success'
      ).then((r) => {
        location.reload();
      })
    }
  
    modifier(): void{
      swal.fire(
        'tarification!',
        'votre tarification est modifier!',
        'success'
      ).then((r) => {
        location.reload();
      })
    }

  private loadScripts() {
    this.dynamicScriptLoader.load('c10','c6','c2','c4','c1','c16').then(data => {
    }).catch(error => console.log(error));
  }
}
