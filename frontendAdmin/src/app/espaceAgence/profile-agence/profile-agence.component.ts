import { Component, OnInit } from '@angular/core';
import { TokenStorageService } from '../../services/token-storage.service';
import { Router } from '@angular/router';
import { AgenceService} from '../../services/agence.service';
import {Agence} from '../../interface/agence';
import swal from 'sweetalert2';

import { HttpClient, HttpHeaders,HttpClientModule,HttpEventType } from '@angular/common/http';




@Component({
  selector: 'app-profile-agence',
  templateUrl: './profile-agence.component.html',
  styleUrls: ['./profile-agence.component.css']
})
export class ProfileAgenceComponent implements OnInit {
agence : Agence;
p;
selectedFile : File = null;


 constructor(private http: HttpClient,private service:AgenceService,private tokenStorage: TokenStorageService,private router: Router) { }

  ngOnInit(): void {
    if (this.tokenStorage.getToken()) {
      console.log(this.tokenStorage.getToken())
      console.log(this.tokenStorage.getUser().Agenceid)
    }
    this.getAgence();
    console.log(this.agence);
  }

 

  getAgence(){
    this.service.getAgenceById(this.tokenStorage.getUser().Agenceid).subscribe(data=>{
     this.agence= data
     console.log(this.agence);


   })
 }

 updateAgence(ag: Agence) {
  this.service.updateAgence(this.tokenStorage.getUser().Agenceid, ag)
    .subscribe(
      response => {
        console.log(response);
        this.succes();
      },
      error => {
        console.log(error);
      });
}

succes(): void{
  swal.fire(
    'Profile!',
    'votre compte est Modifier!',
    'success'
  )
}

updatePassword(ag: Agence) {
  if(ag.password == this.p){
    this.service.updatePassword(this.tokenStorage.getUser().id, ag)
    .subscribe(
      response => {
        console.log(response);
        console.log(ag.password);
        console.log(this.p);
        this.succes1();
      },
      error => {
        console.log(error);
      });
    }
    else{
      this.veriferpassword();
    }
  
  
}

succes1(): void{
  swal.fire(
    'Mot de passe!',
    'votre mot de passe est Modifier!',
    'success'
  )
}

veriferpassword(): void{
  swal.fire({
    icon: 'error',
    title: 'Oops...',
    text: 'Verifier mot de passe!'
  })
}
 

onFileSelected(event){
  this.selectedFile = <File> event.target.files[0];
}

onUpload(){
  const fd = new FormData();
  fd.append('image', this.selectedFile, this.selectedFile.name);
 // console.log(fd);
  //console.log(this.selectedFile.name);
  this.http.post('http://localhost:3000/single', fd)
    .subscribe(res => {
      console.log(res);
    });
  this.agence.image=this.selectedFile.name;
  this.updateAgence(this.agence);

}

}


