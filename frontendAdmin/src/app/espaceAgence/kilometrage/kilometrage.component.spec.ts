import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KilometrageComponent } from './kilometrage.component';

describe('KilometrageComponent', () => {
  let component: KilometrageComponent;
  let fixture: ComponentFixture<KilometrageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KilometrageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KilometrageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
