import { Component, OnInit } from '@angular/core';
import { DynamicFileAdminServiceService } from 'src/app/dynamic-file-service.service';
import { VoitureService} from '../../services/voiture.service';
import { KilometrageService} from '../../services/kilometrage.service';
import {Voiture} from '../../interface/voiture';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TokenStorageService } from '../../services/token-storage.service';
import swal from 'sweetalert2';
import { CommonModule } from "@angular/common";
import { Kilometrage} from '../../interface/kilometrage';


@Component({
  selector: 'app-kilometrage',
  templateUrl: './kilometrage.component.html',
  styleUrls: ['./kilometrage.component.css']
})
export class KilometrageComponent implements OnInit {
  voitures:Voiture[]=[];
  kilometrages:Kilometrage[]=[];
  IsFormNonValid=false;
  kilometrage : Kilometrage= {
    id: "",
    voitureImmatricule: "",
    kilometrage: "",
    date: null, 
    Userid: this.tokenStorage.getUser().id,
    agence:this.tokenStorage.getUser().Agenceid

  }
  id="";


  constructor(private kilometrageService: KilometrageService,private dynamicScriptLoader: DynamicFileAdminServiceService,private tokenStorage: TokenStorageService,private service: VoitureService) { }


  ngOnInit() {
   
    
     this.loadScripts();
     this.getAllVoiture();
     this.getAll();
  }

  getAllVoiture(){
    this.service.allDispo().subscribe(data=>{          
      this.voitures=data; 
      console.log(this.voitures);   
   })
 }

 save(kilometrage :Kilometrage) {
  this.kilometrageService.add(this.kilometrage)
  .subscribe(data =>{ 
    console.log(data);
   this.succes();
   

     },
  
  error => {console.log(error);
   this.IsFormNonValid=true;
 });
}

getAll(){
  this.kilometrageService.all().subscribe(data=>{          
    this.kilometrages=data; 
    console.log(this.kilometrages);  
 })
}

public delete(id:String){
   


  swal.fire({
    title: 'Êtes-vous sûr?',
    text: "Vous ne pourrez pas revenir en arrière!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Oui, supprimé!'
  }).then((result) => {
  
    if (result.value) {
      return this.kilometrageService.supprimer(id).subscribe(data=>{
        swal.fire(
          'Effacé!',
          'Votre kilometrage a été supprimé.',
          'success'
        ).then((r) => {
          location.reload();
        })
      })
      

    }
   // location.reload();  

  
})
}


getOne(id){
  this.kilometrageService.getKilometrageById(id).subscribe(data=>{
    
    this.kilometrage=data;   
 })
}

public update(maintenance){
  this.kilometrageService.updateKilometrage(this.kilometrage.id, this.kilometrage)
  .subscribe(
    response => {
      console.log(response);
      this.modifier();
      
    },
    error => {
      console.log(error);
    });
}


initi():void{
  this.kilometrage= {
    id: "",
    voitureImmatricule: "",
    kilometrage: "",
    date: null, 
    Userid: this.tokenStorage.getUser().id,
    agence:this.tokenStorage.getUser().Agenceid

  }
 }
  // js


  succes(): void{
    swal.fire(
      'Kilometrage!',
      'votre Kilometrage est ajouter!',
      'success'
    ).then((r) => {
      location.reload();
    })
  }

  modifier(): void{
    swal.fire(
      'kilometrage!',
      'votre kilometrage est modifier!',
      'success'
    ).then((r) => {
      location.reload();
    })
  }
  

  private loadScripts() {
    this.dynamicScriptLoader.load('c10','c6','c2','c4','c1','c16').then(data => {
    }).catch(error => console.log(error));
  }

}
