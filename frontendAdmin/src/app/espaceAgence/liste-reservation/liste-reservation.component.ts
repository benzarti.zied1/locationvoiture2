import { Component, OnInit } from '@angular/core';
import { DynamicFileAdminServiceService } from 'src/app/dynamic-file-service.service';
import { VoitureService} from '../../services/voiture.service';
import { ReservationService} from '../../services/reservation.service';
import {Voiture} from '../../interface/voiture';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TokenStorageService } from '../../services/token-storage.service';
import swal from 'sweetalert2';
import { CommonModule } from "@angular/common";
import { Reservation} from '../../interface/reservation';

import { ClientService} from '../../services/client.service';
import {Client} from '../../interface/client';


@Component({
  selector: 'app-liste-reservation',
  templateUrl: './liste-reservation.component.html',
  styleUrls: ['./liste-reservation.component.css']
})
export class ListeReservationComponent implements OnInit {
  voitures:Voiture[]=[];
  clients:Client[]=[];
  reservations:Reservation[]=[];
  IsFormNonValid=false;
  reservation: Reservation= {
    id: "",
    voitureImmatricule: "",
    dateReservation: null,
    Periodede: null,
    Periodea: null,
    heurePeriodede: "",
    minutePeriodede: "",
    heurePeriodea: "",
    minutePeriodea: "",
    client: "",
    Userid: this.tokenStorage.getUser().id,
    agence:this.tokenStorage.getUser().Agenceid

  }
  id="";
  constructor(private clientservice: ClientService,private service: VoitureService,private reservationService: ReservationService,private tokenStorage: TokenStorageService,private dynamicScriptLoader: DynamicFileAdminServiceService) { }


  ngOnInit() {
   
    
     this.loadScripts();
     this.getAllVoiture();
     this.getAllClient();
     this.getAll();
  }
  getAllVoiture(){
    this.service.allDispo().subscribe(data=>{          
      this.voitures=data; 
      console.log(this.voitures);   
   })
 }

 getAllClient(){
  this.clientservice.allClient().subscribe(data=>{          
    this.clients=data; 
    console.log(this.clients);   
 })
}

getAll(){
  this.reservationService.all().subscribe(data=>{          
    this.reservations=data; 
 })
}

save(reservation :Reservation) {
 
  this.reservationService.add(this.reservation)
  .subscribe(data =>{ 
    console.log(data);
   this.succes();
   

     },
  
  error => {console.log(error);
   this.IsFormNonValid=true;
 });
}

public delete(id:String){
     
  
  
  swal.fire({
    title: 'Êtes-vous sûr?',
    text: "Vous ne pourrez pas revenir en arrière!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Oui, supprimé!'
  }).then((result) => {
  
    if (result.value) {
      return this.reservationService.supprimer(id).subscribe(data=>{
        swal.fire(
          'Effacé!',
          'Votre reservation a été supprimé.',
          'success'
        ).then((r) => {
          location.reload();
        })
      })
      

    }
   // location.reload();  

  
})
}


getOne(id){
  this.reservationService.getReservationById(id).subscribe(data=>{
    
    this.reservation=data;   
 })
}

public update(reservation){
  this.reservationService.updateReservation(this.reservation.id, this.reservation)
  .subscribe(
    response => {
      console.log(response);
      this.modifier();
      
    },
    error => {
      console.log(error);
    });
}


initi():void{
  this.reservation= {
    id: "",
    voitureImmatricule: "",
    dateReservation: null,
    Periodede: null,
    Periodea: null,
    heurePeriodede: "",
    minutePeriodede: "",
    heurePeriodea: "",
    minutePeriodea: "",
    client: "",
    Userid: this.tokenStorage.getUser().id,
    agence:this.tokenStorage.getUser().Agenceid

  }
 }
  // js


  succes(): void{
    swal.fire(
      'reservation!',
      'votre reservation est ajouter!',
      'success'
    ).then((r) => {
      location.reload();
    })
  }

  modifier(): void{
    swal.fire(
      'reservation!',
      'votre reservation est modifier!',
      'success'
    ).then((r) => {
      location.reload();
    })
  }
  // js

  private loadScripts() {
    this.dynamicScriptLoader.load('c10','c6','c2','c4','c1','c16').then(data => {
    }).catch(error => console.log(error));
  }

}
