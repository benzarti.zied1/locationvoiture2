import { Component, OnInit } from '@angular/core';
import swal from 'sweetalert2';
import { ActivatedRoute, Router } from '@angular/router';
import { AgenceService} from '../../services/agence.service';




@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {

  email;
  success='success';

    constructor(private activeRoute: ActivatedRoute,private service:AgenceService,private router:Router) { }
  

  ngOnInit(): void {
  }

  resetPassword(utilisateur){
   
    this.activeRoute.queryParams
    .subscribe((params) => 
      {this.email=params["email"];
        this.service.changePassword(utilisateur,this.email).subscribe((data)=>{
          if(data["message"]=="success")
          {
            this.succes1();
            this.success=data["message"];
            }
            else this.success=data["message"];
            

        })
        
  });

  }

  succes1(){
    swal.fire(
      'Modification mot de passe !',
      'Vous avez modifier votre mot de passe!',
      'success'
    ).then(()=>{
      window.location.href = "/";

    })
      }

}
