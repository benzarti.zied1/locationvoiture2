import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashbordVoitureComponent } from './dashbord-voiture.component';

describe('DashbordVoitureComponent', () => {
  let component: DashbordVoitureComponent;
  let fixture: ComponentFixture<DashbordVoitureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashbordVoitureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashbordVoitureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
