import { Component, OnInit } from '@angular/core';
import { DynamicFileAdminServiceService } from 'src/app/dynamic-file-service.service';
import { FactureService} from '../../services/facture.service';
import {Facture} from '../../interface/facture';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TokenStorageService } from '../../services/token-storage.service';
import swal from 'sweetalert2';
import { ClientService} from '../../services/client.service';
import {Client} from '../../interface/client';
import { ArticleService} from '../../services/article.service';
import {Article} from '../../interface/article';

import { PayementService} from '../../services/payement.service';
import {Payement} from '../../interface/payement';


@Component({
  selector: 'app-liste-facture',
  templateUrl: './liste-facture.component.html',
  styleUrls: ['./liste-facture.component.css']
})
export class ListeFactureComponent implements OnInit {
  facture : Facture={
    id: "",
    date: "",
    delai : "",
    Prixtotal: "",
    paye: "",
    client:"",
    Userid: this.tokenStorage.getUser().id,
    agence: this.tokenStorage.getUser().Agenceid
  }

  date= new Date();
  factures: Facture[]=[];
  clients:Client[];
  etat:Array<string>=[];
  payment:Payement={
    id: "",
    date: null,
    montant: "",
    mode: "",
    description: "",
    Userid: this.tokenStorage.getUser().id,
    agence: this.tokenStorage.getUser().Agenceid,
    facture: "",
    contrat:""
  }
  id;

  payments:Payement[]=[];
  montantdue:Array<number>=[];

  payet:string;
  p=0;


  constructor(private payementservice: PayementService,private articleservice: ArticleService,private clientservice:ClientService,private tokenStorage: TokenStorageService,private service: FactureService,private dynamicScriptLoader: DynamicFileAdminServiceService) { }


  ngOnInit() {
   
    
     this.loadScripts();
     this.getAllfacture();
     this.getAllclient();
     this.getAllPayement();
  }
  getAllclient(){
    this.clientservice.allClient().subscribe(data=>{          
      this.clients=data; 
      console.log(this.clients);  
   })
  }
  getAllPayement(){
    this.payementservice.all().subscribe(data=>{          
      this.payments=data; 
      console.log(this.payments);   
     
   })
 }

  getAllfacture(){
    this.service.all().subscribe(data=>{          
      this.factures=data; 
      console.log(this.factures);  

      for(let i in data){
        this.montantdue[i]=parseInt(data[i].Prixtotal) - parseInt(data[i].paye)
        
        var date=new Date()
        
        var datefin=new Date(data[i].delai);
       
        var nb = datefin.getTime() - date.getTime();
        var nbjour= nb / (1000 * 3600 * 24);
        if(nbjour>0)
        this.etat[i]="ouvert";
        else
        this.etat[i]="ferme";
      
      }

      console.log(this.montantdue);


   })
  }


  public delete(id:String){
   


    swal.fire({
      title: 'Êtes-vous sûr?',
      text: "Vous ne pourrez pas revenir en arrière!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Oui, supprimé!'
    }).then((result) => {
    
      if (result.value) {
        return this.service.supprimer(id).subscribe(data=>{
          swal.fire(
            'Effacé!',
            'Votre Facture a été supprimé.',
            'success'
          ).then((r) => {
            location.reload();
          })
        })
        
  
      }
     // location.reload();  
  
    
  })
  }

public change(id){
  this.id=id;
}

public payement(id:string){
  
  this.payment.facture=this.facture.id;
  this.payment.contrat=null;
  this.payementservice.add(this.payment)
  .subscribe(data =>{ 
this.p=parseInt(data.montant)+parseInt(this.facture.paye);
this.payet=this.p.toString();
this.facture.paye=this.payet,

    this.service.updatepaye(this.facture,this.facture.id)
  .subscribe(data1 =>{ 
    console.log(data1);
  }),

    console.log(data);
  this.succes();
   
     },
  

  error => {console.log(error);
   //this.IsFormNonValid=true;
 });
}

getOne(id){
  this.service.getFactureById(id).subscribe(data=>{
    
    this.facture=data;   
 })
}
  


  public inpaye(facture : Facture,id:String){
    return this.service.updateinpaye(facture,id).subscribe(()=>{
      location.reload();
  })
  }


  succes(): void{
    swal.fire(
      'Payement!',
      'votre payments est ajouter!',
      'success'
    ).then((r) => {
      location.reload();
    })
  }

  // js

  private loadScripts() {
    this.dynamicScriptLoader.load('c10','c6','c2','c4','c1','c16').then(data => {
    }).catch(error => console.log(error));
  }


}
