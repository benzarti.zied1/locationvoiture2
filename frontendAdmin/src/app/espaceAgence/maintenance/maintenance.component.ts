import { Component, OnInit } from '@angular/core';
import { DynamicFileAdminServiceService } from 'src/app/dynamic-file-service.service';
import { VoitureService} from '../../services/voiture.service';
import { MaintenanceService} from '../../services/maintenance.service';
import {Voiture} from '../../interface/voiture';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TokenStorageService } from '../../services/token-storage.service';
import swal from 'sweetalert2';
import { CommonModule } from "@angular/common";
import { Maintenance} from '../../interface/maintenance';
import { GarageService} from '../../services/garage.service';
import { Garage} from '../../interface/garage';



@Component({
  selector: 'app-maintenance',
  templateUrl: './maintenance.component.html',
  styleUrls: ['./maintenance.component.css']
})
export class MaintenanceComponent implements OnInit {
  voitures:Voiture[]=[];
  garages:Garage[]=[];
  maintenances:Maintenance[]=[];
  IsFormNonValid=false;
  maintenance : Maintenance= {
    id: "",
    voitureImmatricule: "",
    description: "",
    datedebut: null,
    dateretour: null,
    Heuredebut: "",
    minutedebut: "",
    heureretour: "",
    minuteretour: "",
    prix: "",
    carage:"",
    Userid: this.tokenStorage.getUser().id,
    agence:this.tokenStorage.getUser().Agenceid

  }
  id="";

 
 

  constructor(private garageService:GarageService,private maintenanceService: MaintenanceService,private dynamicScriptLoader: DynamicFileAdminServiceService,private tokenStorage: TokenStorageService,private service: VoitureService) { }


  ngOnInit() {
     this.loadScripts();
     this.getAllVoiture();
     this.getAll();
     this.getAllGarage();
  }

  getAllGarage(){
    this.garageService.all().subscribe(data=>{          
      this.garages=data; 
      console.log(this.garages);   
   })
 }

  getAllVoiture(){
    this.service.allDispo().subscribe(data=>{          
      this.voitures=data; 
      console.log(this.voitures);   
   })
 }

 getAll(){
  this.maintenanceService.all().subscribe(data=>{          
    this.maintenances=data; 
    console.log(this.maintenances);  
 })
}

save(maintenance :Maintenance) {
  this.maintenanceService.add(this.maintenance)
  .subscribe(data =>{ 
    maintenance = {
      id: "",
      voitureImmatricule: "",
      description: "",
      datedebut: null,
      dateretour: null,
      Heuredebut: "",
      minutedebut: "",
      heureretour: "",
      minuteretour: "",
      prix: "",
      carage:"",
      Userid: this.tokenStorage.getUser().id,
      agence:this.tokenStorage.getUser().Agenceid
  
    }

   this.succes();
   

     },
  
  error => {console.log(error);
   this.IsFormNonValid=true;
 });
}

public delete(id:String){
   


  swal.fire({
    title: 'Êtes-vous sûr?',
    text: "Vous ne pourrez pas revenir en arrière!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Oui, supprimé!'
  }).then((result) => {
  
    if (result.value) {
      return this.maintenanceService.supprimer(id).subscribe(data=>{
        swal.fire(
          'Effacé!',
          'Votre maintenance a été supprimé.',
          'success'
        ).then((r) => {
          location.reload();
        })
      })
      

    }
   // location.reload();  

  
})
}


getOne(id){
  this.maintenanceService.getMaintenanceById(id).subscribe(data=>{
    
    this.maintenance=data;   
    console.log(this.maintenance); 
 })
}

public update(maintenance){
  this.maintenanceService.updateMaintenance(this.maintenance.id, this.maintenance)
  .subscribe(
    response => {
      console.log(response);
      this.modifier();
      
    },
    error => {
      console.log(error);
    });
    maintenance = {
      id: "",
      voitureImmatricule: "",
      description: "",
      datedebut: null,
      dateretour: null,
      Heuredebut: "",
      minutedebut: "",
      heureretour: "",
      minuteretour: "",
      prix: "",
      Userid: this.tokenStorage.getUser().id,
      agence:this.tokenStorage.getUser().Agenceid
  
    }
}

initi():void{
 this.maintenance = {
    id: "",
    voitureImmatricule: "",
    description: "",
    datedebut: null,
    dateretour: null,
    Heuredebut: "",
    minutedebut: "",
    heureretour: "",
    minuteretour: "",
    prix: "",
    carage:"",
    Userid: this.tokenStorage.getUser().id,
    agence:this.tokenStorage.getUser().Agenceid

  }
}

succes(): void{
  swal.fire(
    'Maintenance!',
    'votre Maintenance est ajouter!',
    'success'
  ).then((r) => {
    location.reload();
  })
}

modifier(): void{
  swal.fire(
    'Maintenance!',
    'votre Maintenance est modifier!',
    'success'
  ).then((r) => {
    location.reload();
  })
}
  // js

  private loadScripts() {
    this.dynamicScriptLoader.load('c10','c6','c2','c4','c1','c16').then(data => {
    }).catch(error => console.log(error));
  }

}
