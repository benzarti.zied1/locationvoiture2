import { Component, OnInit } from '@angular/core';
import { AgenceService} from '../../services/agence.service';
import {Agence} from '../../interface/agence';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
//import swal from 'sweetalert2';
import swal from 'sweetalert2';
import { Router } from '@angular/router';





@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.component.html',
  styleUrls: ['./inscription.component.css']
})
export class InscriptionComponent implements OnInit {

  user: Agence = {
    email: '',
    username: '',
    password:'',
    active:0,
    nom:'',
    tel:'',
    adressepostale:'',
    ville:'',
    tva:'',
    compteboncaire:'',
    image:''
  };
  IsFormValid=false;
  IsFormNonValid=false;
  constructor(private service:AgenceService,private router: Router) { }

  ngOnInit(): void {
   
  }
  
  save(user:Agence) {
    
    console.log(user)
    this.service.add(this.user)
     .subscribe(data =>{ console.log(data);
      this.succes();
      this.lienmenu();
      
     
        },
     
     error => {console.log(error);
      this.IsFormNonValid=true;
    });

     

      //console.log(user);
   // this.user = new User();
   
  }
     
  succes(): void{
    swal.fire(
      'Incription!',
      'votre compte est valide!',
      'success'
    )

  

  }
 

    // display form values on success
    lienmenu(){
     this.router.navigate(['']);
    }

 

}
