import { Component, OnInit } from '@angular/core';
import { DynamicFileAdminServiceService } from 'src/app/dynamic-file-service.service';
import { VoitureService} from '../../services/voiture.service';
import { CarburantService} from '../../services/carburant.service';
import {Voiture} from '../../interface/voiture';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TokenStorageService } from '../../services/token-storage.service';
import swal from 'sweetalert2';
import { CommonModule } from "@angular/common";
import { Carburant} from '../../interface/carburant';


@Component({
  selector: 'app-carburant',
  templateUrl: './carburant.component.html',
  styleUrls: ['./carburant.component.css']
})
export class CarburantComponent implements OnInit {
  voitures:Voiture[]=[];
  carburants:Carburant[]=[];
  IsFormNonValid=false;
  carburant: Carburant={
    id: "",
    voitureImmatricule: "",
    carburant: "",
    date: null, 
    Userid: this.tokenStorage.getUser().id,
    agence:this.tokenStorage.getUser().Agenceid
  }
  id="";




  constructor(private carburantService: CarburantService,private dynamicScriptLoader: DynamicFileAdminServiceService,private tokenStorage: TokenStorageService,private service: VoitureService) { }


  ngOnInit() {
   
    
     this.loadScripts();
     this.getAllVoiture();
     this.getAll();
  }

  getAllVoiture(){
    this.service.allDispo().subscribe(data=>{          
      this.voitures=data; 
      console.log(this.voitures);   
   })
 }

 save(carburant :Carburant) {
  this.carburantService.add(this.carburant)
  .subscribe(data =>{ 
    console.log(data);
   this.succes();
   

     },
  
  error => {console.log(error);
   this.IsFormNonValid=true;
 });
}

getAll(){
  this.carburantService.all().subscribe(data=>{          
    this.carburants=data; 
 })
}

public delete(id:String){
   


  swal.fire({
    title: 'Êtes-vous sûr?',
    text: "Vous ne pourrez pas revenir en arrière!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Oui, supprimé!'
  }).then((result) => {
  
    if (result.value) {
      return this.carburantService.supprimer(id).subscribe(data=>{
        swal.fire(
          'Effacé!',
          'Votre carburant a été supprimé.',
          'success'
        ).then((r) => {
          location.reload();
        })
      })
      

    }
   // location.reload();  

  
})
}


getOne(id){
  this.carburantService.getcarburantById(id).subscribe(data=>{
    
    this.carburant=data;   
 })
}

public update(carburant){
  this.carburantService.update(this.carburant.id, this.carburant)
  .subscribe(
    response => {
      console.log(response);
      this.modifier();
      
    },
    error => {
      console.log(error);
    });
}

initi():void{
  this.carburant={
    id: "",
    voitureImmatricule: "",
    carburant: "",
    date: null, 
    Userid: this.tokenStorage.getUser().id,
    agence:this.tokenStorage.getUser().Agenceid
  }
 }


//alert

succes(): void{
  swal.fire(
    'Carburant!',
    'votre Carburant est ajouter!',
    'success'
  ).then((r) => {
    location.reload();
  })
}

modifier(): void{
  swal.fire(
    'Carburant!',
    'votre Carburant est modifier!',
    'success'
  ).then((r) => {
    location.reload();
  })
}
  // js

  private loadScripts() {
    this.dynamicScriptLoader.load('c10','c6','c2','c4','c1','c16').then(data => {
    }).catch(error => console.log(error));
  }

  

}
