import { Component, OnInit } from '@angular/core';
import { TokenStorageService } from '../../services/token-storage.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  constructor(private tokenStorage: TokenStorageService,private router: Router) { }

  ngOnInit(): void {
    if (this.tokenStorage.getToken()) {
      console.log(this.tokenStorage.getToken())
      console.log(this.tokenStorage.getUser())
    }
    else {
     // this.lienmenu();
    }
  }

  lienmenu(){
    //this.router.navigate(['']);
  }

  logout() {
    this.tokenStorage.signOut();
    //this.router.navigate(['']);

  }
}
