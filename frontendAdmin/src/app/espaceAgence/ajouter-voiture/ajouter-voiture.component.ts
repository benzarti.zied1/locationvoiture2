import { Component, OnInit } from '@angular/core';
import { VoitureService} from '../../services/voiture.service';
import {Voiture} from '../../interface/voiture';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TokenStorageService } from '../../services/token-storage.service';
import swal from 'sweetalert2';
import { HttpClient, HttpHeaders,HttpClientModule,HttpEventType } from '@angular/common/http';

@Component({
  selector: 'app-ajouter-voiture',
  templateUrl: './ajouter-voiture.component.html',
  styleUrls: ['./ajouter-voiture.component.css']
})
export class AjouterVoitureComponent implements OnInit {

  voiture: Voiture={
    id: "",
    immatricule: "",
    annee: "",
    marque: "",
    modele: "",
    categoire: "",
    boite: "",
    clim: "",
    nbporte: 0,
    nbpersonne: 0,
    consommation: "",
    image: "",
    assurance: "",
    datededebutassurance: null,
    datefinassurance: null,
    prixachat: "",
    dateachat: null,
    datemiseencerculation: null,
    disponible: 1,
    Userid: this.tokenStorage.getUser().id,
    Agence: this.tokenStorage.getUser().Agenceid
  }
  IsFormNonValid=false;
  selectedFile : File = null;

  constructor(private http: HttpClient,private tokenStorage: TokenStorageService,private service: VoitureService) { }

  ngOnInit(): void {
  }


  onFileSelected(event){
    this.selectedFile = <File> event.target.files[0];
  }
  onUpload(){
    const fd = new FormData();
    fd.append('image', this.selectedFile, this.selectedFile.name);
   // console.log(fd);
    //console.log(this.selectedFile.name);
    this.http.post('http://localhost:3000/single', fd)
      .subscribe(res => {
        console.log(res);
      });
  this.voiture.image=this.selectedFile.name;
  
  
  }

  save(voiture:Voiture) {
    if(!(this.voiture.image=="")){
      this.onUpload();

    }
    console.log(voiture)
    this.service.add(this.voiture)
     .subscribe(data =>{ console.log(data);
      this.succes();
      
     this.voiture={
      id: "",
      immatricule: "",
      annee: "",
      marque: "",
      modele: "",
      categoire: "",
      boite: "",
      clim: "",
      nbporte: 0,
      nbpersonne: 0,
      consommation: "",
      image: "",
      assurance: "",
      datededebutassurance: null,
      datefinassurance: null,
      prixachat: "",
      dateachat: null,
      datemiseencerculation: null,
      disponible: 1,
      Userid: this.tokenStorage.getUser().id,
      Agence: this.tokenStorage.getUser().Agenceid
     };
     
        },
     
     error => {console.log(error);
      this.IsFormNonValid=true;
      this.erreur();
    });

}

succes(): void{
  swal.fire(
    'Voiture!',
    'votre voiture est ajoute!',
    'success'
  ).then((r) => {
    location.reload();
  })
}

erreur(): void{
  swal.fire({
    icon: 'error',
    title: 'Oops...',
    text: 'Immatricule est deja existe!'
  })
}



}
