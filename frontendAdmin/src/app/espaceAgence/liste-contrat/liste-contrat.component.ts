import { Component, OnInit } from '@angular/core';
import { DynamicFileAdminServiceService } from 'src/app/dynamic-file-service.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TokenStorageService } from '../../services/token-storage.service';
import swal from 'sweetalert2';
import { CommonModule } from "@angular/common";
import { ClientService} from '../../services/client.service';
import {Client} from '../../interface/client';
import { ContratService} from '../../services/contrat.service';
import {Contrat} from '../../interface/contrat';
import { Router } from '@angular/router';

@Component({
  selector: 'app-liste-contrat',
  templateUrl: './liste-contrat.component.html',
  styleUrls: ['./liste-contrat.component.css']
})
export class ListeContratComponent implements OnInit {
  clients:Client[];
  contrats:Contrat[];
  contrat:Contrat;
  constructor(private router:Router,private clientservice:ClientService,private tokenStorage: TokenStorageService,private contartservice:ContratService,private dynamicScriptLoader: DynamicFileAdminServiceService) { }


  ngOnInit() {
   
    
     this.loadScripts();
     this.getAllclient();
     this.getAll();
  }

  getAll(){
    this.contartservice.all().subscribe(data=>{          
      this.contrats=data; 
      console.log(this.contrats);  
   })
  }

  getAllclient(){
    this.clientservice.allClient().subscribe(data=>{          
      this.clients=data; 
      console.log(this.clients);  
   })
  }

  public delete(id:String){
   


    swal.fire({
      title: 'Êtes-vous sûr?',
      text: "Vous ne pourrez pas revenir en arrière!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Oui, supprimé!'
    }).then((result) => {
    
      if (result.value) {
        return this.contartservice.supprimer(id).subscribe(data=>{
          swal.fire(
            'Effacé!',
            'Votre Contrat a été supprimé.',
            'success'
          ).then((r) => {
            location.reload();
          })
        })
        
  
      }
     // location.reload();  
  
    
  })
  }

  public voir(id:String){
    this.router.navigate(['/VoirContrat'], { queryParams: {id:id}});

  }


  public paye(contrat:Contrat,id:String){
    return this.contartservice.updatepaye(contrat,id).subscribe(()=>{
      location.reload();
  })
  }


  public inpaye(contrat:Contrat,id:String){
    return this.contartservice.updateinpaye(contrat,id).subscribe(()=>{
      location.reload();
  })
  }
  // js  'c10','c6','c2','c4','c1','c16','c12','c13'
  // 'c10','c4','c5','c6','c7','c8','c9','c14','c1'
  private loadScripts() {
    this.dynamicScriptLoader.load('c10','c6','c2','c4','c1','c16').then(data => {
    }).catch(error => console.log(error));
  }


}
