import { Component, OnInit } from '@angular/core';
import { DynamicFileAdminServiceService } from 'src/app/dynamic-file-service.service';
import { VoitureService} from '../../services/voiture.service';
import { RendezvousreservationService} from '../../services/rendezvousreservation.service';
import {Voiture} from '../../interface/voiture';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TokenStorageService } from '../../services/token-storage.service';
import swal from 'sweetalert2';
import { CommonModule } from "@angular/common";
import { Rendezvousreservation} from '../../interface/rendezvousreservation';

import { ClientService} from '../../services/client.service';
import {Client} from '../../interface/client';


@Component({
  selector: 'app-rendez-vous-reservation',
  templateUrl: './rendez-vous-reservation.component.html',
  styleUrls: ['./rendez-vous-reservation.component.css']
})
export class RendezVousReservationComponent implements OnInit {
  clients:Client[]=[];
  rendezvousreservations:Rendezvousreservation[]=[];
  IsFormNonValid=false;
  rendezvousreservation: Rendezvousreservation= {
    id: "",
    description: "",
    date: null,
    Heure: "",
    minute: "",
    client: "",
    Userid: this.tokenStorage.getUser().id,
    agence:this.tokenStorage.getUser().Agenceid

  }
  id="";
  constructor(private clientservice: ClientService,private rendezvousService: RendezvousreservationService,private tokenStorage: TokenStorageService,private dynamicScriptLoader: DynamicFileAdminServiceService) { }


  ngOnInit() {
   
    
     this.loadScripts();
     this.getAllClient();
     this.getAll();
  }
  getAllClient(){
    this.clientservice.allClient().subscribe(data=>{          
      this.clients=data; 
      console.log(this.clients);   
   })
  }

  
getAll(){
  this.rendezvousService.all().subscribe(data=>{          
    this.rendezvousreservations=data; 
 })
}

save(rendezvousreservation:RendezvousreservationService) {
 
  this.rendezvousService.add(this.rendezvousreservation)
  .subscribe(data =>{ 
    console.log(data);
   this.succes();
   

     },
  
  error => {console.log(error);
   this.IsFormNonValid=true;
 });
}

public delete(id:String){
     
  
  
  swal.fire({
    title: 'Êtes-vous sûr?',
    text: "Vous ne pourrez pas revenir en arrière!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Oui, supprimé!'
  }).then((result) => {
  
    if (result.value) {
      return this.rendezvousService.supprimer(id).subscribe(data=>{
        swal.fire(
          'Effacé!',
          'Votre rendez-vous a été supprimé.',
          'success'
        ).then((r) => {
          location.reload();
        })
      })
      

    }
   // location.reload();  

  
})
}


getOne(id){
  this.rendezvousService.getrendezvousreservationById(id).subscribe(data=>{
    
    this.rendezvousreservation=data;   
 })
}

public update(rendezvousreservation){
  this.rendezvousService.updaterendezvousreservation(this.rendezvousreservation.id, this.rendezvousreservation)
  .subscribe(
    response => {
      console.log(response);
      this.modifier();
      
    },
    error => {
      console.log(error);
    });
}


initi():void{
  this.rendezvousreservation= {
    id: "",
    description: "",
    date: null,
    Heure: "",
    minute: "",
    client: "",
    Userid: this.tokenStorage.getUser().id,
    agence:this.tokenStorage.getUser().Agenceid

  }
 }
  // js


  succes(): void{
    swal.fire(
      'Rendez-Vous Reservation!',
      'votre Rendez-Vous Reservation est ajouter!',
      'success'
    ).then((r) => {
      location.reload();
    })
  }

  modifier(): void{
    swal.fire(
      'Rendez-Vous Reservation!',
      'votre Rendez-Vous Reservation est modifier!',
      'success'
    ).then((r) => {
      location.reload();
    })
  }
  // js

  private loadScripts() {
    this.dynamicScriptLoader.load('c10','c6','c2','c4','c1','c16','c17').then(data => {
    }).catch(error => console.log(error));
  }

}
