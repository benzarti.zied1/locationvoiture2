import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RendezVousReservationComponent } from './rendez-vous-reservation.component';

describe('RendezVousReservationComponent', () => {
  let component: RendezVousReservationComponent;
  let fixture: ComponentFixture<RendezVousReservationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RendezVousReservationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RendezVousReservationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
