import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashbordReservationComponent } from './dashbord-reservation.component';

describe('DashbordReservationComponent', () => {
  let component: DashbordReservationComponent;
  let fixture: ComponentFixture<DashbordReservationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashbordReservationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashbordReservationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
