import { Component, OnInit } from '@angular/core';
import { DynamicFileAdminServiceService } from 'src/app/dynamic-file-service.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TokenStorageService } from '../../services/token-storage.service';
import swal from 'sweetalert2';
import { CommonModule } from "@angular/common";
import { ArticleService} from '../../services/article.service';
import {Article} from '../../interface/article';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css']
})
export class ArticleComponent implements OnInit {
  articles:Article[]=[];
  IsFormNonValid=false;
  article: Article={
    id: "",
    nom: "",
    prix: "",
    tva: "", 
    prixttc:"",
    cout:"",
    Userid: this.tokenStorage.getUser().id,
    agence:this.tokenStorage.getUser().Agenceid
  }
  id="";
  constructor(private tokenStorage: TokenStorageService,private articleservice: ArticleService,private dynamicScriptLoader: DynamicFileAdminServiceService) { }


  ngOnInit() {
   
    
     this.loadScripts();
     this.getAll();
  }

  save(article :Article) {
    this.articleservice.add(this.article)
    .subscribe(data =>{ 
      console.log(data);
     this.succes();
     
  
       },
    
    error => {console.log(error);
     this.IsFormNonValid=true;
   });
  }
  
  getAll(){
    this.articleservice.all().subscribe(data=>{          
      this.articles=data; 
   })
  }
  
  public delete(id:String){
     
  
  
    swal.fire({
      title: 'Êtes-vous sûr?',
      text: "Vous ne pourrez pas revenir en arrière!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Oui, supprimé!'
    }).then((result) => {
    
      if (result.value) {
        return this.articleservice.supprimer(id).subscribe(data=>{
          swal.fire(
            'Effacé!',
            'Votre article a été supprimé.',
            'success'
          ).then((r) => {
            location.reload();
          })
        })
        
  
      }
     // location.reload();  
  
    
  })
  }
  
  
  getOne(id){
    this.articleservice.getarticleById(id).subscribe(data=>{
      
      this.article=data;   
   })
  }
  
  public update(article){
    this.articleservice.update(this.article.id, this.article)
    .subscribe(
      response => {
        console.log(response);
        this.modifier();
        
      },
      error => {
        console.log(error);
      });
  }
  
  initi():void{
    this.article={
      id: "",
      nom: "",
      prix: "",
      tva: "", 
      prixttc:"",
      cout:"",
      Userid: this.tokenStorage.getUser().id,
      agence:this.tokenStorage.getUser().Agenceid
    }
   }
   calculerprix(){
     var tva = this.article.tva;
     var prix = this.article.prix;
     var prixttc=0;
     prixttc=prixttc+parseInt(prix)+((parseInt(prix)*parseInt(tva))/100);
     this.article.prixttc=prixttc.toString();

     console.log(tva);
     console.log(prix);
     console.log(this.article.prixttc);
   }
  
  //alert
  
  succes(): void{
    swal.fire(
      'Article!',
      'votre article est ajouter!',
      'success'
    ).then((r) => {
      location.reload();
    })
  }
  
  modifier(): void{
    swal.fire(
      'Article!',
      'votre Article est modifier!',
      'success'
    ).then((r) => {
      location.reload();
    })
  }
  // js

  private loadScripts() {
    this.dynamicScriptLoader.load('c10','c6','c2','c4','c1','c16').then(data => {
    }).catch(error => console.log(error));
  }
}
