import { Component, OnInit } from '@angular/core';
import { DynamicFileAdminServiceService } from 'src/app/dynamic-file-service.service';
import { ClientService} from '../../services/client.service';
import {Client} from '../../interface/client';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TokenStorageService } from '../../services/token-storage.service';
import swal from 'sweetalert2';
import { CommonModule } from "@angular/common";

@Component({
  selector: 'app-liste-client',
  templateUrl: './liste-client.component.html',
  styleUrls: ['./liste-client.component.css']
})
export class ListeClientComponent implements OnInit {
  IsFormNonValid=false;
  clients:Client[];
  client:Client={
        id:"",
        nom: "",
        prenom: "",
        cin:"",
        datenaissance:null,
        ville:"",
        codepostale:"",
        tel1: "",
        tel2: "",
        email: "",
        description: "",
        date:null,
        active:"1",
        Userid:this.tokenStorage.getUser().id,
        agence: this.tokenStorage.getUser().Agenceid
  }

  constructor(private clientservice:ClientService,private tokenStorage: TokenStorageService,private dynamicScriptLoader: DynamicFileAdminServiceService) { }



  ngOnInit() {
   
    
     this.loadScripts();
     this.getAll();
  }
  save(client :Client) {
    this.clientservice.addClient(this.client)
    .subscribe(data =>{ 
      console.log(data);
     this.succes();
     
  
       },
    
    error => {console.log(error);
     this.IsFormNonValid=true;
   });
  }

  getAll(){
    this.clientservice.allClient().subscribe(data=>{          
      this.clients=data; 
      console.log(this.clients);  
   })
  }
  public delete(id:String){
   


    swal.fire({
      title: 'Êtes-vous sûr?',
      text: "Vous ne pourrez pas revenir en arrière!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Oui, supprimé!'
    }).then((result) => {
    
      if (result.value) {
        return this.clientservice.supprimer(id).subscribe(data=>{
          swal.fire(
            'Effacé!',
            'Votre Client a été supprimé.',
            'success'
          ).then((r) => {
            location.reload();
          })
        })
        
  
      }
     // location.reload();  
  
    
  })
  }

  getOne(id){
    this.clientservice.getclientById(id).subscribe(data=>{
      
      this.client=data;   
   })
  }
  
  
  public update(client){
    this.clientservice.update(this.client.id, this.client)
    .subscribe(
      response => {
        console.log(response);
        this.modifier();
        
      },
      error => {
        console.log(error);
      });
  }

  initi():void{
    this.client={
      id:"",
      nom: "",
      prenom: "",
      cin:"",
      datenaissance:null,
      ville:"",
      codepostale:"",
      tel1: "",
      tel2: "",
      email: "",
      description: "",
      date:null,
      active:"1",
      Userid:this.tokenStorage.getUser().id,
      agence: this.tokenStorage.getUser().Agenceid
}
   }

  succes(): void{
    swal.fire(
      'Client!',
      'votre Client est ajouter!',
      'success'
    ).then((r) => {
      location.reload();
    })
  }

  modifier(): void{
    swal.fire(
      'Client!',
      'votre Client est modifier!',
      'success'
    ).then((r) => {
      location.reload();
    })
  }
  // js

  private loadScripts() {
    this.dynamicScriptLoader.load('c10','c6','c2','c4','c1','c16').then(data => {
    }).catch(error => console.log(error));
  }


}
