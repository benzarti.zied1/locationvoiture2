import { Component, OnInit } from '@angular/core';
import { DynamicFileAdminServiceService } from 'src/app/dynamic-file-service.service';
import { GarageService} from '../../services/garage.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TokenStorageService } from '../../services/token-storage.service';
import swal from 'sweetalert2';
import { CommonModule } from "@angular/common";
import { Garage} from '../../interface/garage';


@Component({
  selector: 'app-garage',
  templateUrl: './garage.component.html',
  styleUrls: ['./garage.component.css']
})
export class GarageComponent implements OnInit {
  garages:Garage[]=[];
  IsFormNonValid=false;
  garage : Garage= {
    id: "",
    nom: "",
    tel: "",
    adresse: "", 
    Userid: this.tokenStorage.getUser().id,
    agence:this.tokenStorage.getUser().Agenceid

  }
  id="";
  constructor(private garageServices: GarageService,private tokenStorage: TokenStorageService,private dynamicScriptLoader: DynamicFileAdminServiceService) { }

  ngOnInit(): void {
    this.loadScripts();
    this.getAll();
  }

  save(garage :Garage) {
    this.garageServices.add(this.garage)
    .subscribe(data =>{ 
      console.log(data);
     this.succes();
     
  
       },
    
    error => {console.log(error);
     this.IsFormNonValid=true;
   });
  }
  
  getAll(){
    this.garageServices.all().subscribe(data=>{          
      this.garages=data; 
      console.log(this.garages);  
   })
  }
  
  public delete(id:String){
     
  
  
    swal.fire({
      title: 'Êtes-vous sûr?',
      text: "Vous ne pourrez pas revenir en arrière!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Oui, supprimé!'
    }).then((result) => {
    
      if (result.value) {
        return this.garageServices.supprimer(id).subscribe(data=>{
          swal.fire(
            'Effacé!',
            'Votre garage a été supprimé.',
            'success'
          ).then((r) => {
            location.reload();
          })
        })
        
  
      }
     // location.reload();  
  
    
  })
  }
  
  
  getOne(id){
    this.garageServices.getgarageById(id).subscribe(data=>{
      
      this.garage=data;   
   })
  }
  
  public update(garage){
    this.garageServices.updategarage(this.garage.id, this.garage)
    .subscribe(
      response => {
        console.log(response);
        this.modifier();
        
      },
      error => {
        console.log(error);
      });
  }
  
  
  initi():void{
    this.garage= {
      id: "",
    nom: "",
    tel: "",
    adresse: "", 
    Userid: this.tokenStorage.getUser().id,
    agence:this.tokenStorage.getUser().Agenceid
  
    }
   }
    // js
  
  
    succes(): void{
      swal.fire(
        'Garage!',
        'votre garage est ajouter!',
        'success'
      ).then((r) => {
        location.reload();
      })
    }
  
    modifier(): void{
      swal.fire(
        'Garage!',
        'votre garage est modifier!',
        'success'
      ).then((r) => {
        location.reload();
      })
    }


  private loadScripts() {
    this.dynamicScriptLoader.load('c10','c6','c2','c4','c1','c16').then(data => {
    }).catch(error => console.log(error));
  } 
}
