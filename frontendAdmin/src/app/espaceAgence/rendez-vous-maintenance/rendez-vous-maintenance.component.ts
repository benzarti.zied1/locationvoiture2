import { Component, OnInit } from '@angular/core';
import { DynamicFileAdminServiceService } from 'src/app/dynamic-file-service.service';
import { VoitureService} from '../../services/voiture.service';
import { RendezvousmaintenanceService} from '../../services/rendezvousmaintenance.service';
import {Voiture} from '../../interface/voiture';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TokenStorageService } from '../../services/token-storage.service';
import swal from 'sweetalert2';
import { CommonModule } from "@angular/common";
import { Rendezvousmaintenance} from '../../interface/rendezvousmaintenance';
import { GarageService} from '../../services/garage.service';
import { Garage} from '../../interface/garage';


@Component({
  selector: 'app-rendez-vous-maintenance',
  templateUrl: './rendez-vous-maintenance.component.html',
  styleUrls: ['./rendez-vous-maintenance.component.css']
})
export class RendezVousMaintenanceComponent implements OnInit {
  voitures:Voiture[]=[];
  garages:Garage[]=[];

  rendezvousmaintenances:Rendezvousmaintenance[]=[];
  IsFormNonValid=false;
  rendezvousmaintenance: Rendezvousmaintenance= {
    id: "",
    voitureImmatricule: "",
    description: "",
    datedebut: null,
    dateretour: null,
    Heuredebut: "",
    minutedebut: "",
    heureretour: "",
    minuteretour: "",
    carage: "",
    Userid: this.tokenStorage.getUser().id,
    agence:this.tokenStorage.getUser().Agenceid

  }
  id="";

  constructor(private garageService:GarageService,private rendezvousService: RendezvousmaintenanceService,private dynamicScriptLoader: DynamicFileAdminServiceService,private tokenStorage: TokenStorageService,private service: VoitureService) { }


  ngOnInit() {
   
    
     this.loadScripts();
     this.getAllVoiture();
     this.getAll();
     this.getAllGarage();
  }

  getAllGarage(){
    this.garageService.all().subscribe(data=>{          
      this.garages=data; 
      console.log(this.garages);   
   })
 }
  getAllVoiture(){
    this.service.allDispo().subscribe(data=>{          
      this.voitures=data; 
      console.log("voiture");
      console.log(this.voitures);   
   })
 }

 getAll(){
  this.rendezvousService.all().subscribe(data=>{          
    this.rendezvousmaintenances=data; 
    console.log(this.rendezvousmaintenances);  
 })
}

save(rendezvousmaintenance :Rendezvousmaintenance) {
  this.rendezvousService.add(this.rendezvousmaintenance)
  .subscribe(data =>{ 
    rendezvousmaintenance= {
      id: "",
      voitureImmatricule: "",
      description: "",
      datedebut: null,
      dateretour: null,
      Heuredebut: "",
      minutedebut: "",
      heureretour: "",
      minuteretour: "",
      carage: "",
      Userid: this.tokenStorage.getUser().id,
      agence:this.tokenStorage.getUser().Agenceid
  
    }

   this.succes();
   

     },
  
  error => {console.log(error);
   this.IsFormNonValid=true;
 });
}

public delete(id:String){
   


  swal.fire({
    title: 'Êtes-vous sûr?',
    text: "Vous ne pourrez pas revenir en arrière!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Oui, supprimé!'
  }).then((result) => {
  
    if (result.value) {
      return this.rendezvousService.supprimer(id).subscribe(data=>{
        swal.fire(
          'Effacé!',
          'Votre Rendez-vous a été supprimé.',
          'success'
        ).then((r) => {
          location.reload();
        })
      })
      

    }
   // location.reload();  

  
})
}


getOne(id){
  this.rendezvousService.getrendezvousmaintenanceById(id).subscribe(data=>{
    
    this.rendezvousmaintenance=data;   
    console.log(this.rendezvousmaintenance); 
 })
}

public update(rendezvousmaintenance){
  this.rendezvousService.updaterendezvousmaintenance(this.rendezvousmaintenance.id, this.rendezvousmaintenance)
  .subscribe(
    response => {
      console.log(response);
      this.modifier();
      this.initi();
      
    },
    error => {
      console.log(error);
    });
 
}

initi():void{
  this.rendezvousmaintenance= {
    id: "",
    voitureImmatricule: "",
    description: "",
    datedebut: null,
    dateretour: null,
    Heuredebut: "",
    minutedebut: "",
    heureretour: "",
    minuteretour: "",
    carage: "",
    Userid: this.tokenStorage.getUser().id,
    agence:this.tokenStorage.getUser().Agenceid

  }
}

succes(): void{
  swal.fire(
    'Rendez-vous!',
    'votre Rendez-vous est ajouter!',
    'success'
  ).then((r) => {
    location.reload();
  })
}

modifier(): void{
  swal.fire(
    'Rendez-vous!',
    'votre Rendez-vous est modifier!',
    'success'
  ).then((r) => {
    location.reload();
  })
}

  // js

  private loadScripts() {
    this.dynamicScriptLoader.load('c10','c6','c2','c4','c1','c16','c18').then(data => {
    }).catch(error => console.log(error));
  }

}
