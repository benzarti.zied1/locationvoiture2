import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RendezVousMaintenanceComponent } from './rendez-vous-maintenance.component';

describe('RendezVousMaintenanceComponent', () => {
  let component: RendezVousMaintenanceComponent;
  let fixture: ComponentFixture<RendezVousMaintenanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RendezVousMaintenanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RendezVousMaintenanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
