import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListefacturecontratComponent } from './listefacturecontrat.component';

describe('ListefacturecontratComponent', () => {
  let component: ListefacturecontratComponent;
  let fixture: ComponentFixture<ListefacturecontratComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListefacturecontratComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListefacturecontratComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
