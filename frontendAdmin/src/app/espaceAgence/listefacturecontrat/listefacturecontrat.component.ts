import { Component, OnInit } from '@angular/core';
import { DynamicFileAdminServiceService } from 'src/app/dynamic-file-service.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TokenStorageService } from '../../services/token-storage.service';
import swal from 'sweetalert2';
import { CommonModule } from "@angular/common";
import { ClientService} from '../../services/client.service';
import {Client} from '../../interface/client';
import { ContratService} from '../../services/contrat.service';
import {Contrat} from '../../interface/contrat';
import { Router } from '@angular/router';
import { PayementService} from '../../services/payement.service';
import {Payement} from '../../interface/payement';
@Component({
  selector: 'app-listefacturecontrat',
  templateUrl: './listefacturecontrat.component.html',
  styleUrls: ['./listefacturecontrat.component.css']
})
export class ListefacturecontratComponent implements OnInit {

  clients:Client[];
  contrats:Contrat[];
  contrat:Contrat;
  payment:Payement={
    id: "",
    date: null,
    montant: "",
    mode: "",
    description: "",
    Userid: this.tokenStorage.getUser().id,
    agence: this.tokenStorage.getUser().Agenceid,
    facture: "",
    contrat:""
  }
  montantdue:Array<number>=[];

  payet:string;
  p=0;
  constructor(private payementservice: PayementService,private router:Router,private clientservice:ClientService,private tokenStorage: TokenStorageService,private contartservice:ContratService,private dynamicScriptLoader: DynamicFileAdminServiceService) { }


  ngOnInit() {
   
    
     this.loadScripts();
     this.getAllclient();
     this.getAll();
  }

  getAll(){
    this.contartservice.all().subscribe(data=>{          
      this.contrats=data; 
      console.log(this.contrats);  
      for(let i in data){
        this.montantdue[i]=parseInt(data[i].prixtotal) - parseInt(data[i].paye)
        
        
      
      }

      console.log(this.montantdue);


   })
  }

  getAllclient(){
    this.clientservice.allClient().subscribe(data=>{          
      this.clients=data; 
      console.log(this.clients);  
   })
  }

  public delete(id:String){
   


    swal.fire({
      title: 'Êtes-vous sûr?',
      text: "Vous ne pourrez pas revenir en arrière!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Oui, supprimé!'
    }).then((result) => {
    
      if (result.value) {
        return this.contartservice.supprimer(id).subscribe(data=>{
          swal.fire(
            'Effacé!',
            'Votre Contrat a été supprimé.',
            'success'
          ).then((r) => {
            location.reload();
          })
        })
        
  
      }
     // location.reload();  
  
    
  })
  }

  public voir(id:String){
    this.router.navigate(['/VoirContrat'], { queryParams: {id:id}});

  }


  getOne(id){
    this.contartservice.getContratById(id).subscribe(data=>{
      
      this.contrat=data;   
   })
  }

  

  public payement(payement: Payement){
  
    this.payment.facture=null;
    this.payment.contrat=this.contrat.id;
    console.log(this.payment);
    this.payementservice.add(this.payment)
   
    .subscribe(data =>{ 
  this.p=parseInt(data.montant)+parseInt(this.contrat.paye);
  this.payet=this.p.toString();
  this.contrat.paye=this.payet,
  
      this.contartservice.updatepaye(this.contrat,this.contrat.id)
    .subscribe(data1 =>{ 
      console.log(data1);
    }),
  
      console.log(data);
    this.succes();
     
       },
    
  
    error => {console.log(error);
     //this.IsFormNonValid=true;
   });
  }  

  succes(): void{
    swal.fire(
      'Payement!',
      'votre payments est ajouter!',
      'success'
    ).then((r) => {
      location.reload();
    })
  }
 
  // js  'c10','c6','c2','c4','c1','c16','c12','c13'
  // 'c10','c4','c5','c6','c7','c8','c9','c14','c1'
  private loadScripts() {
    this.dynamicScriptLoader.load('c10','c6','c2','c4','c1','c16').then(data => {
    }).catch(error => console.log(error));
  }


}
