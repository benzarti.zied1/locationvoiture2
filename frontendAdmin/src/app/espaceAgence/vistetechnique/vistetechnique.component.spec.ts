import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VistetechniqueComponent } from './vistetechnique.component';

describe('VistetechniqueComponent', () => {
  let component: VistetechniqueComponent;
  let fixture: ComponentFixture<VistetechniqueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VistetechniqueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VistetechniqueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
