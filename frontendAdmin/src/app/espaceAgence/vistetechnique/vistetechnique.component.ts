import { Component, OnInit } from '@angular/core';
import { DynamicFileAdminServiceService } from 'src/app/dynamic-file-service.service';
import { VoitureService} from '../../services/voiture.service';
import { VistetechniqueService} from '../../services/vistetechnique.service';
import {Voiture} from '../../interface/voiture';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TokenStorageService } from '../../services/token-storage.service';
import swal from 'sweetalert2';
import { CommonModule } from "@angular/common";
import { Vistetechnique} from '../../interface/vistetechnique';
import { GarageService} from '../../services/garage.service';
import { Garage} from '../../interface/garage';




@Component({
  selector: 'app-vistetechnique',
  templateUrl: './vistetechnique.component.html',
  styleUrls: ['./vistetechnique.component.css']
})
export class VistetechniqueComponent implements OnInit {
  garages:Garage[]=[];
  voitures:Voiture[]=[];
  vistetechniques:Vistetechnique[]=[];
  IsFormNonValid=false;
  vistetechnique : Vistetechnique= {
    id: "",
    voitureImmatricule: "",
    description: "",
    kilometrage:"",
    prix: "",
    carage: "",    
    date: null,
    Userid: this.tokenStorage.getUser().id,
    agence:this.tokenStorage.getUser().Agenceid

  }
  id="";
  constructor(private garageService:GarageService,private vistetechniqueService: VistetechniqueService,private dynamicScriptLoader: DynamicFileAdminServiceService,private tokenStorage: TokenStorageService,private service: VoitureService) { }

  ngOnInit(): void {
    this.loadScripts();
    this.getAllVoiture();
    this.getAll();
    this.getAllGarage();
  }

  getAllGarage(){
    this.garageService.all().subscribe(data=>{          
      this.garages=data; 
      console.log(this.garages);   
   })
 }

  getAllVoiture(){
    this.service.allDispo().subscribe(data=>{          
      this.voitures=data; 
      console.log(this.voitures);   
   })
 }

 save(vistetechnique :Vistetechnique) {
  this.vistetechniqueService.add(this.vistetechnique)
  .subscribe(data =>{ 
    console.log(data);
   this.succes();
   

     },
  
  error => {console.log(error);
   this.IsFormNonValid=true;
 });
}

getAll(){
  this.vistetechniqueService.all().subscribe(data=>{          
    this.vistetechniques=data; 
    console.log(this.vistetechniques);  
 })
}

public delete(id:String){
   


  swal.fire({
    title: 'Êtes-vous sûr?',
    text: "Vous ne pourrez pas revenir en arrière!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Oui, supprimé!'
  }).then((result) => {
  
    if (result.value) {
      return this.vistetechniqueService.supprimer(id).subscribe(data=>{
        swal.fire(
          'Effacé!',
          'Votre Viste technique a été supprimé.',
          'success'
        ).then((r) => {
          location.reload();
        })
      })
      

    }
   // location.reload();  

  
})
}


getOne(id){
  this.vistetechniqueService.getvistetechniqueById(id).subscribe(data=>{
    
    this.vistetechnique=data;  
    this.vistetechnique.carage=data['GarageId'];
    console.log(this.vistetechnique);
 })
}


public update(vistetechnique){
  this.vistetechniqueService.updatevistetechnique(this.vistetechnique.id, this.vistetechnique)
  .subscribe(
    response => {
      console.log(response);
      this.modifier();
      
    },
    error => {
      console.log(error);
    });
}


initi():void{
  this.vistetechnique= {
    id: "",
    voitureImmatricule: "",
    description: "",
    kilometrage:"",
    prix: "",
    carage: "",    
    date: null,
    Userid: this.tokenStorage.getUser().id,
    agence:this.tokenStorage.getUser().Agenceid

  }
 }
  // js


  succes(): void{
    swal.fire(
      'vistetechnique!',
      'votre vistetechnique est ajouter!',
      'success'
    ).then((r) => {
      location.reload();
    })
  }

  modifier(): void{
    swal.fire(
      'vistetechnique!',
      'votre vistetechnique est modifier!',
      'success'
    ).then((r) => {
      location.reload();
    })
  }
  

  private loadScripts() {
    this.dynamicScriptLoader.load('c10','c6','c2','c4','c1','c16').then(data => {
    }).catch(error => console.log(error));
  }
}
