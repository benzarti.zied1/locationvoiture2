import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListeOptionComponent } from './liste-option.component';

describe('ListeOptionComponent', () => {
  let component: ListeOptionComponent;
  let fixture: ComponentFixture<ListeOptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListeOptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListeOptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
