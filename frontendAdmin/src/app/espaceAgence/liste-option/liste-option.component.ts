import { Component, OnInit } from '@angular/core';
import { DynamicFileAdminServiceService } from 'src/app/dynamic-file-service.service';
import { OptionService} from '../../services/option.service';
import {Option} from '../../interface/option';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TokenStorageService } from '../../services/token-storage.service';
import swal from 'sweetalert2';
@Component({
  selector: 'app-liste-option',
  templateUrl: './liste-option.component.html',
  styleUrls: ['./liste-option.component.css']
})
export class ListeOptionComponent implements OnInit {
options:Option[]=[];
IsFormNonValid=false;
option : Option= {
    id: "",
    nom: "",
    prix: "",
    Userid: this.tokenStorage.getUser().id,
    agence:this.tokenStorage.getUser().Agenceid

}
id="";
  constructor(private optionService: OptionService,private dynamicScriptLoader: DynamicFileAdminServiceService,private tokenStorage: TokenStorageService) { }

  ngOnInit(): void {
    this.loadScripts();
    this.getAll();
  }

  save(option :Option) {
    this.optionService.add(this.option)
    .subscribe(data =>{ 
      console.log(data);
     this.succes();
     
  
       },
    
    error => {console.log(error);
     this.IsFormNonValid=true;
   });
  }
  
  getAll(){
    this.optionService.all().subscribe(data=>{          
      this.options=data; 
      console.log(this.options);  
   })
  }
  
  public delete(id:String){
     
  
  
    swal.fire({
      title: 'Êtes-vous sûr?',
      text: "Vous ne pourrez pas revenir en arrière!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Oui, supprimé!'
    }).then((result) => {
    
      if (result.value) {
        return this.optionService.supprimer(id).subscribe(data=>{
          swal.fire(
            'Effacé!',
            'Votre Option a été supprimé.',
            'success'
          ).then((r) => {
            location.reload();
          })
        })
        
  
      }
     // location.reload();  
  
    
  })
  }
  
  
  getOne(id){
    this.optionService.getOptionById(id).subscribe(data=>{
      
      this.option=data;   
   })
  }
  
  public update(option){
    this.optionService.updateOption(this.option.id, this.option)
    .subscribe(
      response => {
        console.log(response);
        this.modifier();
        
      },
      error => {
        console.log(error);
      });
  }
  
  
  initi():void{
    this.option= {
      id: "",
      nom: "",
      prix: "",
      Userid: this.tokenStorage.getUser().id,
      agence:this.tokenStorage.getUser().Agenceid
  
  }
   }
    // js
  
  
    succes(): void{
      swal.fire(
        'Option!',
        'votre Option est ajouter!',
        'success'
      ).then((r) => {
        location.reload();
      })
    }
  
    modifier(): void{
      swal.fire(
        'Option!',
        'votre Option est modifier!',
        'success'
      ).then((r) => {
        location.reload();
      })
    }

  private loadScripts() {
    this.dynamicScriptLoader.load('c10','c6','c2','c4','c1','c16').then(data => {
    }).catch(error => console.log(error));
  }

}
