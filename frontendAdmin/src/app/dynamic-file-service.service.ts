import { Injectable } from '@angular/core';

interface Scripts {
  name: string;
  src: string;
}

export const ScriptStore: Scripts[] = [
  
  // modal
  { name: 'c1', src: 'assets/app-assets/js/scripts/advance-ui-modals.min.js' },
  //script pour datatable important c2 et c6 et c10   
  { name: 'c2', src: 'assets/app-assets/js/scripts/data-tables.min.js' },
  { name: 'c3', src: 'assets/app-assets/vendors/data-tables/js/dataTables.select.min.js' },

  { name: 'c4', src: 'assets/app-assets/vendors/data-tables/js/jquery.dataTables.min.js' },
  { name: 'c5', src: 'assets/app-assets/vendors/data-tables/js/datatables.checkboxes.min.js' },

  { name: 'c6', src: 'assets/app-assets/js/plugins.min.js' },
  { name: 'c7', src: 'assets/app-assets/js/search.min.js' },
  { name: 'c8', src: 'assets/app-assets/js/custom/custom-script.min.js' },

  { name: 'c9', src: 'assets/app-assets/js/scripts/customizer.min.js' },
  { name: 'c10', src: 'assets/app-assets/js/vendors.min.js' },

  //script pour  calendar
  { name: 'c11', src: 'assets/app-assets/js/scripts/app-calendar.min.js' },


   //script pour  select search 
   { name: 'c12', src: 'assets/app-assets/js/scripts/form-select2.min.js' },
   { name: 'c13', src: 'assets/app-assets/vendors/select2/select2.full.min.js' },

   //script pour  datatable invoice en component contrat

   { name: 'c14', src: 'assets/app-assets/js/scripts/app-invoice.min.js' },



   //ajouter ligne facture
   { name: 'c15', src: 'assets/app-assets/js/facture.js' },


   { name: 'c16', src: 'assets/app-assets/js/sel.js' },

   { name: 'c17', src: 'assets/app-assets/js/scripts/app-reservation.js' },
   { name: 'c18', src: 'assets/app-assets/js/scripts/app-calendar.min.js' },



   { name: 'c19', src:'https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js' },
   



 






   









 



  // steps
  { name: 's1', src: 'assets/js/main-steps.js' }

  ];







declare var document: any;

@Injectable()
export class DynamicFileAdminServiceService {

  private scripts: any = {};

  constructor() {
    ScriptStore.forEach((script: any) => {
      this.scripts[script.name] = {
        loaded: false,
        src: script.src
      };
    });
    // 
   
  }

  load(...scripts: string[]) {
    const promises: any[] = [];
    scripts.forEach((script) => promises.push(this.loadScript(script)));
    return Promise.all(promises);
  }

  loadScript(name: string) {
    return new Promise((resolve, reject) => {
      if (!this.scripts[name].loaded) {
        //load script
        let script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = this.scripts[name].src;
       // debut code xxxxxx 
       // cet code permet d'importer les fichiers pour chaque access au page  
        script.onreadystatechange = () => {
            if (script.readyState === "loaded" || script.readyState === "complete") {
                script.onreadystatechange = null;
                this.scripts[name].loaded = true;
                resolve({script: name, loaded: true, status: 'Loaded'});
            }
        };
        // fin xxxxxxxxxxxxxxx
        console.log(name);
        script.onerror = (error: any) => resolve({script: name, loaded: false, status: 'Loaded'});
        document.getElementsByTagName('body')[0].appendChild(script);
      } else {
        resolve({ script: name, loaded: true, status: 'Already Loaded' });
      }
    });
  }

  // 


  

}