import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {Routes,RouterModule} from '@angular/router';
import { HttpClientModule } from '@angular/common/http'; 
import { FormsModule }   from '@angular/forms';
import { AuthInterceptorService } from './services/auth-interceptor.service';
import { EmployeService} from './services/employe.service';
import { CommonModule } from '@angular/common';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './espaceAgence/navbar/navbar.component';
import { SidebarComponent } from './espaceAgence/sidebar/sidebar.component';
import { FooterComponent } from './espaceAgence/footer/footer.component';
import { DashbordVoitureComponent } from './espaceAgence/dashbord-voiture/dashbord-voiture.component';
import { DashbordReservationComponent } from './espaceAgence/dashbord-reservation/dashbord-reservation.component';
import { DashbordCompteComponent } from './espaceAgence/dashbord-compte/dashbord-compte.component';
import { DashbordFactureComponent } from './espaceAgence/dashbord-facture/dashbord-facture.component';
import { ListeReservationComponent } from './espaceAgence/liste-reservation/liste-reservation.component';
import { ListeSouhaiteComponent } from './espaceAgence/liste-souhaite/liste-souhaite.component';
import { RendezVousReservationComponent } from './espaceAgence/rendez-vous-reservation/rendez-vous-reservation.component';
import { ListevoitureDisponibleComponent } from './espaceAgence/listevoiture-disponible/listevoiture-disponible.component';
import { ListeContactsComponent } from './espaceAgence/liste-contacts/liste-contacts.component';
import { ListeClientComponent } from './espaceAgence/liste-client/liste-client.component';
import { AjouterContratComponent } from './espaceAgence/ajouter-contrat/ajouter-contrat.component';
import { ListeContratComponent } from './espaceAgence/liste-contrat/liste-contrat.component';
import { AjouterVoitureComponent } from './espaceAgence/ajouter-voiture/ajouter-voiture.component';
import { ListeVoitureNonDispoComponent } from './espaceAgence/liste-voiture-non-dispo/liste-voiture-non-dispo.component';
import { MaintenanceComponent } from './espaceAgence/maintenance/maintenance.component';
import { KilometrageComponent } from './espaceAgence/kilometrage/kilometrage.component';
import { CarburantComponent } from './espaceAgence/carburant/carburant.component';
import { DiagosticComponent } from './espaceAgence/diagostic/diagostic.component';
import { RendezVousMaintenanceComponent } from './espaceAgence/rendez-vous-maintenance/rendez-vous-maintenance.component';
import { AjouterFactureComponent } from './espaceAgence/ajouter-facture/ajouter-facture.component';
import { ListeFactureComponent } from './espaceAgence/liste-facture/liste-facture.component';
import { ListeUserAgenceComponent } from './espaceAgence/liste-user-agence/liste-user-agence.component';
import { ListeUserComponent } from './espaceAgence/liste-user/liste-user.component';
import { ProfileAgenceComponent } from './espaceAgence/profile-agence/profile-agence.component';
import { TraqueursComponent } from './espaceAgence/traqueurs/traqueurs.component';
import { ContraventionComponent } from './espaceAgence/contravention/contravention.component';
import { MenuComponent } from './espaceAgence/menu/menu.component';
import { VoirContratComponent } from './espaceAgence/voir-contrat/voir-contrat.component';
import { DynamicFileAdminServiceService } from './dynamic-file-service.service';
import { InscriptionComponent } from './espaceAgence/inscription/inscription.component';
import { LoginComponent } from './espaceAgence/login/login.component';
import { ArticleComponent } from './espaceAgence/article/article.component';
import { ListedemandeComponent } from './espaceAgence/listedemande/listedemande.component';
import {AgenceService} from './services/agence.service';
import { PasswordComponent } from './espaceAgence/password/password.component';
import { ResetPasswordComponent } from './espaceAgence/reset-password/reset-password.component';
import { VistetechniqueComponent } from './espaceAgence/vistetechnique/vistetechnique.component';
import { GarageComponent } from './espaceAgence/garage/garage.component';
import { ListeOptionComponent } from './espaceAgence/liste-option/liste-option.component';
import { TarificationComponent } from './espaceAgence/tarification/tarification.component';
import { ContratsimpleComponent } from './espaceAgence/contratsimple/contratsimple.component';
import { VoirfactureComponent } from './espaceAgence/voirfacture/voirfacture.component';
import { DashborddocumentComponent } from './espaceAgence/dashborddocument/dashborddocument.component';
import { ListedocumenttraiteComponent } from './espaceAgence/listedocumenttraite/listedocumenttraite.component';
import { ListeagenceComponent } from './espaceAgence/listeagence/listeagence.component';
import { ListedocumentnonComponent } from './espaceAgence/listedocumentnon/listedocumentnon.component';
import { ListepaymentComponent } from './espaceAgence/listepayment/listepayment.component';
import { PaymentComponent } from './espaceAgence/payment/payment.component';
import { ListefacturecontratComponent } from './espaceAgence/listefacturecontrat/listefacturecontrat.component';

const routes:Routes=[
  {path:'DashbordVoiture',component:DashbordVoitureComponent},
  {path:'DashbordCompte',component:DashbordCompteComponent},
  {path:'DashbordReservation',component:DashbordReservationComponent},
  {path:'DashbordFacture',component:DashbordFactureComponent},

  {path:'ListeReservation',component:ListeReservationComponent},
  {path:'ListeSouhaite',component:ListeSouhaiteComponent },
  {path:'RendezVousReservation',component:RendezVousReservationComponent },
  {path:'ListevoitureDisponible',component:ListevoitureDisponibleComponent },
  {path:'ListeContacts',component:ListeContactsComponent},
  {path:'ListeClient',component:ListeClientComponent},
  {path:'AjouterContrat',component:AjouterContratComponent},
  {path:'ListeContrat',component:ListeContratComponent},
  {path:'VoirContrat',component:VoirContratComponent},
  {path:'Listedemande',component:ListedemandeComponent},
  {path:'Listeoption',component:ListeOptionComponent},
  {path:'tarification',component:TarificationComponent},
  {path:'Contratsimple',component:ContratsimpleComponent},

  {path:'VoirFacture',component:VoirfactureComponent},


  



  {path:'AjouterVoiture',component:AjouterVoitureComponent},
  {path:'ListeVoitureNonDispo',component:ListeVoitureNonDispoComponent},
  {path:'Maintenance',component:MaintenanceComponent},
  {path:'Kilometrage',component:KilometrageComponent},
  {path:'Carburant',component:CarburantComponent},
  {path:'Diagostic',component:DiagosticComponent},
  {path:'RendezVousMaintenance',component:RendezVousMaintenanceComponent},
  {path:'Traqueurs',component:TraqueursComponent},
  {path:'Vistetechnique',component:VistetechniqueComponent},
  {path:'Garage',component:GarageComponent},

  {path:'Contravention',component:ContraventionComponent},

  {path:'AjouterFacture',component:AjouterFactureComponent},
  {path:'ListeFacture',component:ListeFactureComponent},
  {path:'Article',component:ArticleComponent},


  {path:'ListeUser',component:ListeUserComponent},
  {path:'ProfileAgence',component:ProfileAgenceComponent},

  {path:'Menu',component:MenuComponent},

  {path:'Inscription',component:InscriptionComponent },
  {path:'',component:LoginComponent},


  {path:'ListUserAgence',component:ListeUserAgenceComponent },
  {path:'Password',component:PasswordComponent},
  {path:'RestePassword',component:ResetPasswordComponent},


  {path:'Dashborddocument',component:DashborddocumentComponent},
  {path:'listedocumenttraite',component:ListedocumenttraiteComponent},

  //{path:'Listedocumentnon',loadChildren:() => DocumentModule },

  {path:'Listedocumentnon',component:ListedocumentnonComponent},

  {path:'Listeagence',component:ListeagenceComponent},


  {path:'Listepayment',component:ListepaymentComponent},
  {path:'Payment',component:PaymentComponent},

  {path:'Listefacturecontrat',component:ListefacturecontratComponent},














]
@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    SidebarComponent,
    FooterComponent,
    DashbordVoitureComponent,
    DashbordReservationComponent,
    DashbordCompteComponent,
    DashbordFactureComponent,
    ListeReservationComponent,
    ListeSouhaiteComponent,
    RendezVousReservationComponent,
    ListevoitureDisponibleComponent,
    ListeContactsComponent,
    ListeClientComponent,
    AjouterContratComponent,
    ListeContratComponent,
    AjouterVoitureComponent,
    ListeVoitureNonDispoComponent,
    MaintenanceComponent,
    KilometrageComponent,
    CarburantComponent,
    DiagosticComponent,
    RendezVousMaintenanceComponent,
    AjouterFactureComponent,
    ListeFactureComponent,
    ListeUserComponent,
    ProfileAgenceComponent,
    TraqueursComponent,
    ContraventionComponent,
    MenuComponent,
    VoirContratComponent,
    InscriptionComponent,
    LoginComponent,
    ArticleComponent,
    ListedemandeComponent,
    PasswordComponent,
    ResetPasswordComponent,
    VistetechniqueComponent,
    GarageComponent,
    ListeOptionComponent,
    TarificationComponent,
    ContratsimpleComponent,
    VoirfactureComponent,
    DashborddocumentComponent,
    ListedocumenttraiteComponent,
    ListedocumentnonComponent,
    ListeagenceComponent,
    ListepaymentComponent,
    PaymentComponent,
    ListefacturecontratComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    CommonModule,
    FormsModule,
    RouterModule.forRoot(routes)
  ],
  providers: [DynamicFileAdminServiceService,AuthInterceptorService,AgenceService,EmployeService],
  bootstrap: [AppComponent]
})
export class AppModule { }
