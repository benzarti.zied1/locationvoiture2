import { TestBed } from '@angular/core/testing';

import { VistetechniqueService } from './vistetechnique.service';

describe('VistetechniqueService', () => {
  let service: VistetechniqueService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(VistetechniqueService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
