import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Contrat } from '../interface/contrat';
import { Facture } from '../interface/facture';
@Injectable({
  providedIn: 'root'
})
export class FactureService {

  public host:string="http://localhost:3000/api/facture/";

  constructor(private httpClient:HttpClient) { }
  public add(facture:Facture ){
    return this.httpClient.post<Facture >(this.host+"create",facture);
  }


  public all():Observable<Facture[]>{
    return this.httpClient.get<Facture[]>(this.host+"findall");
  }

  public supprimer(id:String):Observable<void>{
    return this.httpClient.delete<void>(this.host+"delete/"+id);
  }

  getFactureById(id: string): Observable<Facture> {
    return this.httpClient.get<Facture>(this.host+"findOne/" + id);
  }


  getFacturearticle(id: string): Observable<any> {
    return this.httpClient.get<any>(this.host+"findfacture/" + id);
  }

  public updatepaye(facture:Facture,id: String){
    return this.httpClient.put<Facture>(this.host+"paye/"+id,facture);
  }

  public updateinpaye(facture:Facture,id: String){
    return this.httpClient.put<Facture>(this.host+"inpaye/"+id,facture);
  }

  

}
