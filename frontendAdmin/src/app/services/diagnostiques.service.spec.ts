import { TestBed } from '@angular/core/testing';

import { DiagnostiquesService } from './diagnostiques.service';

describe('DiagnostiquesService', () => {
  let service: DiagnostiquesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DiagnostiquesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
