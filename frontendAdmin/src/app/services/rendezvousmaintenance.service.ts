import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Rendezvousmaintenance } from '../interface/rendezvousmaintenance';

@Injectable({
  providedIn: 'root'
})
export class RendezvousmaintenanceService {
  public host:string="http://localhost:3000/api/Rendezvousmaintenance/";

  constructor(private httpClient:HttpClient) { }
  

  public add(rendezvousmaintenance:Rendezvousmaintenance ){
    return this.httpClient.post<Rendezvousmaintenance>(this.host+"create",rendezvousmaintenance);
  }

  public all():Observable<Rendezvousmaintenance[]>{
    return this.httpClient.get<Rendezvousmaintenance[]>(this.host+"findall");
  }

  public supprimer(id:String):Observable<void>{
    return this.httpClient.delete<void>(this.host+"delete/"+id);
  }

  getrendezvousmaintenanceById(id: string): Observable<Rendezvousmaintenance> {
    return this.httpClient.get<Rendezvousmaintenance>(this.host+"findOne/" + id);
  }

  updaterendezvousmaintenance(id, rendezvousmaintenance) {
    return this.httpClient.put(this.host+"update/"+id,rendezvousmaintenance);
  }
}
