import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Payement } from '../interface/payement';
@Injectable({
  providedIn: 'root'
})
export class PayementService {
  public host:string="http://localhost:3000/api/payement/";

  constructor(private httpClient:HttpClient) { }

  public add(payement:Payement ){
    return this.httpClient.post<Payement>(this.host+"create",payement);
  }

  public all():Observable<Payement[]>{
    return this.httpClient.get<Payement[]>(this.host+"findall");
  }

  public supprimer(id:String):Observable<void>{
    return this.httpClient.delete<void>(this.host+"delete/"+id);
  }

  getPayementById(id: string): Observable<Payement> {
    return this.httpClient.get<Payement>(this.host+"findOne/" + id);
  }

  updatePayement(id, payement) {
    return this.httpClient.put(this.host+"update/"+id,payement);
  }

 
}
