import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Tarification } from '../interface/tarification';
@Injectable({
  providedIn: 'root'
})
export class TarificationService {
  public host:string="http://localhost:3000/api/tarification/";

  constructor(private httpClient:HttpClient) { }
  public add(tarification:Tarification ){
    return this.httpClient.post<Tarification>(this.host+"create",tarification);
  }

  public all():Observable<Tarification[]>{
    return this.httpClient.get<Tarification[]>(this.host+"findall");
  }

  public supprimer(id:String):Observable<void>{
    return this.httpClient.delete<void>(this.host+"delete/"+id);
  }

  gettarificationById(id: string): Observable<Tarification> {
    return this.httpClient.get<Tarification>(this.host+"findOne/" + id);
  }

  updatetarification(id, tarification) {
    return this.httpClient.put(this.host+"update/"+id,tarification);
  }

  getverifier(id: string): Observable<Tarification> {
    return this.httpClient.get<Tarification>(this.host+"find/" + id);
  }
}
