import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Vistetechnique } from '../interface/vistetechnique';

@Injectable({
  providedIn: 'root'
})
export class VistetechniqueService {
  public host:string="http://localhost:3000/api/vistetechnique/";

  constructor(private httpClient:HttpClient) { }

  public add(vistetechnique:Vistetechnique ){
    return this.httpClient.post<Vistetechnique>(this.host+"create",vistetechnique);
  }

  public all():Observable<Vistetechnique[]>{
    return this.httpClient.get<Vistetechnique[]>(this.host+"findall");
  }

  public supprimer(id:String):Observable<void>{
    return this.httpClient.delete<void>(this.host+"delete/"+id);
  }

  getvistetechniqueById(id: string): Observable<Vistetechnique> {
    return this.httpClient.get<Vistetechnique>(this.host+"findOne/" + id);
  }

  updatevistetechnique(id, vistetechnique) {
    return this.httpClient.put(this.host+"update/"+id,vistetechnique);
  }

}
