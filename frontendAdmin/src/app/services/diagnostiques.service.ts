import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Diagnostiques } from '../interface/diagnostiques';

@Injectable({
  providedIn: 'root'
})
export class DiagnostiquesService {
  public host:string="http://localhost:3000/api/diagnostiques/";


  constructor(private httpClient:HttpClient) { }
  public add(diagnostiques:Diagnostiques ){
    return this.httpClient.post<Diagnostiques>(this.host+"create",diagnostiques);
  }

  public all():Observable<Diagnostiques[]>{
    return this.httpClient.get<Diagnostiques[]>(this.host+"findall");
  }

  public supprimer(id:String):Observable<void>{
    return this.httpClient.delete<void>(this.host+"delete/"+id);
  }

  getdiagnostiquesById(id: string): Observable<Diagnostiques> {
    return this.httpClient.get<Diagnostiques>(this.host+"findOne/" + id);
  }

  updatediagnostiques(id, diagnostiques) {
    return this.httpClient.put(this.host+"update/"+id,diagnostiques);
  }
}
