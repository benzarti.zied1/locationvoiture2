import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Rendezvousreservation } from '../interface/rendezvousreservation';
@Injectable({
  providedIn: 'root'
})
export class RendezvousreservationService {

  public host:string="http://localhost:3000/api/rendezvousreservation/";

  constructor(private httpClient:HttpClient) { }
  

  public add(rendezvousreservation:Rendezvousreservation ){
    return this.httpClient.post<Rendezvousreservation>(this.host+"create",rendezvousreservation);
  }

  public all():Observable<Rendezvousreservation[]>{
    return this.httpClient.get<Rendezvousreservation[]>(this.host+"findall");
  }

  public supprimer(id:String):Observable<void>{
    return this.httpClient.delete<void>(this.host+"delete/"+id);
  }

  getrendezvousreservationById(id: string): Observable<Rendezvousreservation> {
    return this.httpClient.get<Rendezvousreservation>(this.host+"findOne/" + id);
  }

  updaterendezvousreservation(id, rendezvousreservation) {
    return this.httpClient.put(this.host+"update/"+id,rendezvousreservation);
  }
}
