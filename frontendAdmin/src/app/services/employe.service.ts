import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Employe } from '../interface/employe';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmployeService {

  public host:string="http://localhost:3000/api/auth/";
 

  constructor( private httpClient:HttpClient) { }

  public add(employe: Employe){
    return this.httpClient.post<Employe>(this.host+"signupemployee",employe);
  }
  public all():Observable<Employe[]>{
    return this.httpClient.get<Employe[]>(this.host+"Affiche");
      }
  public supprimer(id:String):Observable<void>{
    return this.httpClient.delete<void>(this.host+"SuppEmploye/"+id);
  }
  
  public active(employe: Employe,id: String){
    return this.httpClient.put<Employe>(this.host+"updateActive/"+id,employe);
  }

  public desactive(employe: Employe,id: String){
    return this.httpClient.put<Employe>(this.host+"upadteDesactive/"+id,employe);
  }
  

  

}
