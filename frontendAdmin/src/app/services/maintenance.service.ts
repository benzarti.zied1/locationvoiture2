import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Maintenance } from '../interface/maintenance';

@Injectable({
  providedIn: 'root'
})
export class MaintenanceService {

  public host:string="http://localhost:3000/api/maintenance/";
  constructor(private httpClient:HttpClient) { }

  public add(maintenance:Maintenance ){
    return this.httpClient.post<Maintenance>(this.host+"create",maintenance);
  }

  public all():Observable<Maintenance[]>{
    return this.httpClient.get<Maintenance[]>(this.host+"findall");
  }

  public supprimer(id:String):Observable<void>{
    return this.httpClient.delete<void>(this.host+"delete/"+id);
  }

  getMaintenanceById(id: string): Observable<Maintenance> {
    return this.httpClient.get<Maintenance>(this.host+"findOne/" + id);
  }

  updateMaintenance(id, maintenance) {
    return this.httpClient.put(this.host+"update/"+id,maintenance);
  }
}
