import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import {Article  } from '../interface/article';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {
  public host:string="http://localhost:3000/api/article/";

  constructor(private httpClient:HttpClient) { }

  public add(article:Article ){
    return this.httpClient.post<Article>(this.host+"create",article);
  }

  public all():Observable<Article[]>{
    return this.httpClient.get<Article[]>(this.host+"findall");
  }

  public supprimer(id:String):Observable<void>{
    return this.httpClient.delete<void>(this.host+"delete/"+id);
  }

  getarticleById(id: string): Observable<Article> {
    return this.httpClient.get<Article>(this.host+"findOne/" + id);
  }

  update(id, article) {
    return this.httpClient.put(this.host+"update/"+id,article);
  }
}
