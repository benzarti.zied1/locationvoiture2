import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Kilometrage } from '../interface/kilometrage';

@Injectable({
  providedIn: 'root'
})
export class KilometrageService {
  public host:string="http://localhost:3000/api/kilometrage/";

  constructor(private httpClient:HttpClient) { }
  public add(kilometrage:Kilometrage ){
    return this.httpClient.post<Kilometrage>(this.host+"create",kilometrage);
  }

  public all():Observable<Kilometrage[]>{
    return this.httpClient.get<Kilometrage[]>(this.host+"findall");
  }

  public supprimer(id:String):Observable<void>{
    return this.httpClient.delete<void>(this.host+"delete/"+id);
  }

  getKilometrageById(id: string): Observable<Kilometrage> {
    return this.httpClient.get<Kilometrage>(this.host+"findOne/" + id);
  }

  updateKilometrage(id, kilometrage) {
    return this.httpClient.put(this.host+"update/"+id,kilometrage);
  }


}
