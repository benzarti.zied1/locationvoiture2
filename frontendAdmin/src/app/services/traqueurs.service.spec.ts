import { TestBed } from '@angular/core/testing';

import { TraqueursService } from './traqueurs.service';

describe('TraqueursService', () => {
  let service: TraqueursService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TraqueursService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
