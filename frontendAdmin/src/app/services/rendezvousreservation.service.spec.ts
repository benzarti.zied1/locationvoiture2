import { TestBed } from '@angular/core/testing';

import { RendezvousreservationService } from './rendezvousreservation.service';

describe('RendezvousreservationService', () => {
  let service: RendezvousreservationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RendezvousreservationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
