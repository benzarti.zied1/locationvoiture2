import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Contravention } from '../interface/contravention';

@Injectable({
  providedIn: 'root'
})
export class ContraventionService {

  public host:string="http://localhost:3000/api/contravention/";

  constructor(private httpClient:HttpClient) { }
  public add(contravention:Contravention ){
    return this.httpClient.post<Contravention>(this.host+"create",contravention);
  }

  public all():Observable<Contravention[]>{
    return this.httpClient.get<Contravention[]>(this.host+"findall");
  }

  public supprimer(id:String):Observable<void>{
    return this.httpClient.delete<void>(this.host+"delete/"+id);
  }

  getContraventionById(id: string): Observable<Contravention> {
    return this.httpClient.get<Contravention>(this.host+"findOne/" + id);
  }

  updateContravention(id, contravention) {
    return this.httpClient.put(this.host+"update/"+id,contravention);
  }
}
