import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Client } from '../interface/client';

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  public host:string="http://localhost:3000/api/client/";

  constructor(private httpClient:HttpClient) { }

  public addClient(client:Client ){
    return this.httpClient.post<Client>(this.host+"createclient",client);
  }

  public addContact(client:Client ){
    return this.httpClient.post<Client>(this.host+"createcontact",client);
  }

  public supprimer(id:String):Observable<void>{
    return this.httpClient.delete<void>(this.host+"delete/"+id);
  }

  getclientById(id: string): Observable<Client> {
    return this.httpClient.get<Client>(this.host+"findOne/" + id);
  }

  update(id, client) {
    return this.httpClient.put(this.host+"update/"+id,client);
  }

  public allClient():Observable<Client[]>{
    return this.httpClient.get<Client[]>(this.host+"findallclient");
  }

  public allContact():Observable<Client[]>{
    return this.httpClient.get<Client[]>(this.host+"findallcontact");
  }


}
