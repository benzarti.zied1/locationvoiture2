import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Carburant } from '../interface/carburant';

@Injectable({
  providedIn: 'root'
})
export class CarburantService {
  public host:string="http://localhost:3000/api/carburant/";

  constructor(private httpClient:HttpClient) { }

  public add(carburant:Carburant ){
    return this.httpClient.post<Carburant>(this.host+"create",carburant);
  }

  public all():Observable<Carburant[]>{
    return this.httpClient.get<Carburant[]>(this.host+"findall");
  }

  public supprimer(id:String):Observable<void>{
    return this.httpClient.delete<void>(this.host+"delete/"+id);
  }

  getcarburantById(id: string): Observable<Carburant> {
    return this.httpClient.get<Carburant>(this.host+"findOne/" + id);
  }

  update(id, carburant) {
    return this.httpClient.put(this.host+"update/"+id,carburant);
  }

}
