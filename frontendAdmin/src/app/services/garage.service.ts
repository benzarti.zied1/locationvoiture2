import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Garage } from '../interface/garage';

@Injectable({
  providedIn: 'root'
})
export class GarageService {

  public host:string="http://localhost:3000/api/garage/";

  constructor(private httpClient:HttpClient) { }
  public add(garage:Garage ){
    return this.httpClient.post<Garage>(this.host+"create",garage);
  }

  public all():Observable<Garage[]>{
    return this.httpClient.get<Garage[]>(this.host+"findall");
  }

  public supprimer(id:String):Observable<void>{
    return this.httpClient.delete<void>(this.host+"delete/"+id);
  }

  getgarageById(id: string): Observable<Garage> {
    return this.httpClient.get<Garage>(this.host+"findOne/" + id);
  }

  updategarage(id, garage) {
    return this.httpClient.put(this.host+"update/"+id,garage);
  }
}
