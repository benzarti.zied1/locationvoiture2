import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Option } from '../interface/option';

@Injectable({
  providedIn: 'root'
})
export class OptionService {
  public host:string="http://localhost:3000/api/option/";

  constructor(private httpClient:HttpClient) { }
  public add(option:Option ){
    return this.httpClient.post<Option>(this.host+"create",option);
  }

  public all():Observable<Option[]>{
    return this.httpClient.get<Option[]>(this.host+"findall");
  }

  public supprimer(id:String):Observable<void>{
    return this.httpClient.delete<void>(this.host+"delete/"+id);
  }

  getOptionById(id: string): Observable<Option> {
    return this.httpClient.get<Option>(this.host+"findOne/" + id);
  }

  updateOption(id, option) {
    return this.httpClient.put(this.host+"update/"+id,option);
  }
}
