import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Voiture } from '../interface/voiture';


@Injectable({
  providedIn: 'root'
})
export class VoitureService {

  public host:string="http://localhost:3000/api/voiture/";
  constructor(private httpClient:HttpClient) { }

  public add(voiture:Voiture ){
    return this.httpClient.post<Voiture>(this.host+"create",voiture);
  }
  public allDispo():Observable<Voiture[]>{
    return this.httpClient.get<Voiture[]>(this.host+"affichedispo");
  }

  public allNonDispo():Observable<Voiture[]>{
    return this.httpClient.get<Voiture[]>(this.host+"affichenondispo");
  }

  public updateNonDisponible(voiture: Voiture,id: String){
    return this.httpClient.put<Voiture>(this.host+"updateNonDisponbile/"+id,voiture);
  }

  public updateDisponible(voiture: Voiture,id: String){
    return this.httpClient.put<Voiture>(this.host+"updateDisponible/"+id,voiture);
  }

  public supprimer(id:String):Observable<void>{
    return this.httpClient.delete<void>(this.host+"delete/"+id);
  }

  getVoitureById(id: string): Observable<Voiture> {
    return this.httpClient.get<Voiture>(this.host+"voiturefindOne/" + id);
  }

  updateVoiture(id, voiture) {
    return this.httpClient.put(this.host+"update/"+id,voiture);
  }
}
