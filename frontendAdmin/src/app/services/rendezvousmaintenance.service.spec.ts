import { TestBed } from '@angular/core/testing';

import { RendezvousmaintenanceService } from './rendezvousmaintenance.service';

describe('RendezvousmaintenanceService', () => {
  let service: RendezvousmaintenanceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RendezvousmaintenanceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
