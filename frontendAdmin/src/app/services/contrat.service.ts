import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Contrat } from '../interface/contrat';
import { Contratoption } from '../interface/contratoption';


@Injectable({
  providedIn: 'root'
})
export class ContratService {

  public host:string="http://localhost:3000/api/contrat/";

  constructor(private httpClient:HttpClient) { }
  public add(contrat:Contrat ){
    return this.httpClient.post<Contrat>(this.host+"create",contrat);
  }
  public all():Observable<Contrat[]>{
    return this.httpClient.get<Contrat[]>(this.host+"findall");
  }
  public supprimer(id:String):Observable<void>{
    return this.httpClient.delete<void>(this.host+"delete/"+id);
  }
  getContratById(id: string): Observable<Contrat> {
    return this.httpClient.get<Contrat>(this.host+"findOne/" + id);
  }

  getVoiture(id: string): Observable<any> {
    return this.httpClient.get<any>(this.host+"findVoiture/" + id);
  }

  public updatepaye(contrat:Contrat,id: String){
    return this.httpClient.put<Contrat>(this.host+"paye/"+id,contrat);
  }

  public updateinpaye(contrat:Contrat,id: String){
    return this.httpClient.put<Contrat>(this.host+"inpaye/"+id,contrat);
  }
  

}
