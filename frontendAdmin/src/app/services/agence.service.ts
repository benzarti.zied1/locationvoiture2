import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Agence  } from '../interface/agence';
import { Observable } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class AgenceService {

 
  public host:string="http://localhost:3000/api/auth/";
  public hostgetemail:string="http://localhost:3000/api/auth/getCode/";
  public hostchangePassword:string="http://localhost:3000/api/auth/changePassword/";



  constructor(private httpClient:HttpClient) { }

  public add(agence:Agence ){
    return this.httpClient.post<Agence >(this.host+"signupa",agence);
  }

  changePassword(utilisateur,email):Observable<any>{
    return this.httpClient.put<any>(this.hostchangePassword+email,utilisateur);
  }

 
  login1(credentials): Observable<any> {
    return this.httpClient.post(this.host+"signin" , {
      username: credentials.username,
      password: credentials.password
    }, httpOptions);
  }

  getAgenceById(id: string): Observable<Agence> {
    return this.httpClient.get<Agence>(this.host+"agencefindOne/" + id);
  }

  updateAgence(id, data) {
    return this.httpClient.put(this.host+"updateAgence/"+id,data);
  }

  updatePassword(id, data) {
    return this.httpClient.put(this.host+"updatePassword/"+id,data);
  }

  getCode(email):Observable<any>{
    return this.httpClient.get<any>(this.hostgetemail+email);
  }


  public allagence():Observable<any[]>{
    return this.httpClient.get<any[]>(this.host+"Affichetoutagence");
  }


  public alluser():Observable<any[]>{
    return this.httpClient.get<any[]>(this.host+"Affichetoutuser");
  }

  public updateactive(agence:Agence,id: String){
    return this.httpClient.put<Agence>(this.host+"active/"+id,agence);
  }

  public updatedesactive(agence:Agence,id: String){
    return this.httpClient.put<Agence>(this.host+"desactive/"+id,agence);
  }
  public supprimer(id:String):Observable<void>{
    return this.httpClient.delete<void>(this.host+"SuppAgence/"+id);
  }
}
