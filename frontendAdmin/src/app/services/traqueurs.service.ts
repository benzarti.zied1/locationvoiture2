import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Traqueurs } from '../interface/traqueurs';

@Injectable({
  providedIn: 'root'
})
export class TraqueursService {
  public host:string="http://localhost:3000/api/traqueurs/";

  constructor(private httpClient:HttpClient) { }
  public add(traqueurs:Traqueurs ){
    return this.httpClient.post<Traqueurs>(this.host+"create",traqueurs);
  }

  public all():Observable<Traqueurs[]>{
    return this.httpClient.get<Traqueurs[]>(this.host+"findall");
  }

  public supprimer(id:String):Observable<void>{
    return this.httpClient.delete<void>(this.host+"delete/"+id);
  }

  getTraqueursById(id: string): Observable<Traqueurs> {
    return this.httpClient.get<Traqueurs>(this.host+"findOne/" + id);
  }

  updateTraqueurs(id, traqueurs) {
    return this.httpClient.put(this.host+"update/"+id,traqueurs);
  }
}
