import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Reservation } from '../interface/reservation';

@Injectable({
  providedIn: 'root'
})
export class ReservationService {

  public host:string="http://localhost:3000/api/reservation/";

  constructor(private httpClient:HttpClient) { }
  public add(reservation:Reservation ){
    return this.httpClient.post<Reservation>(this.host+"create",reservation);
  }

  public all():Observable<Reservation[]>{
    return this.httpClient.get<Reservation[]>(this.host+"findall");
  }

  public supprimer(id:String):Observable<void>{
    return this.httpClient.delete<void>(this.host+"delete/"+id);
  }

  getReservationById(id: string): Observable<Reservation> {
    return this.httpClient.get<Reservation>(this.host+"findOne/" + id);
  }

  updateReservation(id, reservation) {
    return this.httpClient.put(this.host+"update/"+id,reservation);
  }
}
