export interface Diagnostiques {
    id: string,
    voitureImmatricule: string,
    prix: string,
    description: string,
    carage: string,
    date: Date,
    Userid: string,
    agence: string
}
