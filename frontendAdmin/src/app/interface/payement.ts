export interface Payement {
    id: string,
    date: Date,
    montant: string,
    mode: string,
    description: string,
    Userid: string,
    agence: string,
    facture: string,
    contrat:string
}
