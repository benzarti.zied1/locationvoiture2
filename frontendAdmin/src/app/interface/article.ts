export interface Article {
    id: string,
    nom: string,
    prix : string,
    tva: string,
    prixttc: string,
    cout:string,
    Userid: string,
    agence: string
}
