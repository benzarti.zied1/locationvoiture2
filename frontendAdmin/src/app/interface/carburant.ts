export interface Carburant {
    id: string,
    voitureImmatricule: string,
    carburant : string,
    date: Date,
    Userid: string,
    agence: string
}
