export interface Contrat {
id: string,
periodede: Date,
periodea: Date,
date: Date,
commentaire: string,
acompte: string,
prixvoiture: string,
prixoption: string,
prixtotal: string,
client: string,
Userid: string,
paye:string,
optionId: any[],
agence: string
}
