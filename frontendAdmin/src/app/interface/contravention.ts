export interface Contravention {
    id: string,
    voitureImmatricule: string,
    dateafraction: Date,
    montant: string,
    client: string,
    Userid: string,
    agence: string
}
