export interface Garage {
    id: string,
    nom: string,
    tel: string,
    adresse: string,
    Userid: string,
    agence: string
}
