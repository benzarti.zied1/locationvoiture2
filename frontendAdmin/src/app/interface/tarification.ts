export interface Tarification {
    id: string,
    categoire: string,
    prix: string,
    kilometrage: string,
    Userid: string,
    agence: string
}
