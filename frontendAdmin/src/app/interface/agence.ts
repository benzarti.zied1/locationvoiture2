export interface Agence {
    username:string,
    email:string,
    password:string,
    active:number,
    nom:string,
    tel:string,
    adressepostale:string,
    ville:string,
    tva:string,
    compteboncaire:string,
    image:string
}
