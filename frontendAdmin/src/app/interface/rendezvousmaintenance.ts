export interface Rendezvousmaintenance {
id: string,
voitureImmatricule: string,
description: string,
datedebut: Date,
dateretour: Date,
Heuredebut: string,
minutedebut: string,
heureretour: string,
minuteretour: string,
carage: string,
Userid: string,
agence: string
}
