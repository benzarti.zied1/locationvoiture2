export interface Client {
        id:string,
        nom: string,
        prenom: string,
        cin:string,
        datenaissance:Date,
        ville:string,
        codepostale:string,
        tel1: string,
        tel2: string,
        email: string,
        description: string,
        date:Date,
        active:string,
        Userid:string,
        agence: string 
}
