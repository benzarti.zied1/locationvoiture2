export interface Maintenance {
id: string,
voitureImmatricule: string,
description: string,
datedebut: Date,
dateretour: Date,
Heuredebut: string,
minutedebut: string,
heureretour: string,
minuteretour: string,
prix: string,
Userid: string,
carage:string,
agence: string
}
