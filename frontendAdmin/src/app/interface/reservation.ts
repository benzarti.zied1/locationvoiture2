export interface Reservation {
    id: string,
    voitureImmatricule: string,
    dateReservation: Date,
    Periodede: Date,
    Periodea: Date,
    heurePeriodede: string,
    minutePeriodede: string,
    heurePeriodea: string,
    minutePeriodea: string,
    client: string,
    Userid: string,
    agence: string
}
