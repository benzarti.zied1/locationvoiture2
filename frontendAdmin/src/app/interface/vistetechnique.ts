export interface Vistetechnique {
    id: string,
    voitureImmatricule: string,
    description: string,
    kilometrage: string,
    prix: string,
    carage: string,    
    date: Date,
    Userid: string,
    agence: string
}
