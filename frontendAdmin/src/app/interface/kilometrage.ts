export interface Kilometrage {
    id: string,
    voitureImmatricule: string,
    kilometrage: string,
    date: Date,
    Userid: string,
    agence: string
}
